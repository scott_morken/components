@props(['title' => 'Info', 'content'])
<a tabindex="0"
   role="button"
   {{ $attributes->class(['popover-dismiss']) }}
   data-bs-toggle="popover"
   data-bs-trigger="focus"
   data-bs-title="{{ $title }}"
   data-bs-content="{{ $content }}">{{ !$slot->isEmpty() ? $slot : $title }}</a>
