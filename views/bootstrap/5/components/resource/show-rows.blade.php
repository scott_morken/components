<?php
/**
 * @var \Smorken\Domain\ViewModels\Contracts\RetrieveViewModel $viewModel
 * @var \Smorken\Support\Contracts\Filter|\Smorken\QueryStringFilter\Contracts\QueryStringFilter $filter
 * @var \Smorken\Model\Contracts\Model $model
 */
use Smorken\Support\Filter;

$filter = $viewModel->filter() ?? new Filter();
$model = $viewModel->model();
?>
@props([
    'viewModel',
    'title' => null,
    'extra' => [],
    'limitAttributes' => [],
    'actions' => true,
    'backActionMethod' => 'index',
    'firstCol' => 'col-sm-3 col-md-2',
    'secondCol' => 'col-sm-9 col-md-10',
])
@php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
@if($title)
    <x-smc::resource.title-back :actionMethod="$backActionMethod" :filter="$filter" :params="$params">{{ $title }}</x-smc::resource.title-back>
@endif
<h5 class="mb-2">View record [{{ $model->getKey() }}]</h5>
@if ($actions)
    <div class="row mb-4">
        <div class="col">
            <x-smc::resource.action.edit-button class="btn-block w-100"
                                                :params="$params"></x-smc::resource.action.edit-button>
        </div>
        <div class="col">
            <x-smc::resource.action.confirm-delete-button class="btn-block w-100"
                                                          :params="$params"></x-smc::resource.action.confirm-delete-button>
        </div>
    </div>
@endif
@isset($prepend)
    {{ $prepend }}
@endisset
@if ($slot->isEmpty())
    <x-smc::resource.part.show-attribute-rows :model="$model" :limitAttributes="$limitAttributes" :extra="$extra"
                                              :firstCol="$firstCol"
                                              :secondCol="$secondCol"></x-smc::resource.part.show-attribute-rows>
@else
    {{ $slot }}
@endif
@isset($append)
    {{ $append }}
@endisset
