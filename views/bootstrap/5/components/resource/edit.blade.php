<?php
/**
 * @var \Smorken\Domain\ViewModels\Contracts\RetrieveViewModel $viewModel
 */
use Smorken\Support\Filter;

$model = $viewModel->model();
$filter = $viewModel->filter() ?? new Filter();
?>
@props([
    'viewModel',
    'title' => null,
    'formActionMethod' => 'update',
    'cancelController' => $controller,
    'cancelActionMethod' => 'index',
    'formView' => null,
])
@php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
@if($title)
    <x-smc::resource.title-back>{{ $title }}</x-smc::resource.title-back>
@endif
<h5 class="mb-2">Update record [{{ $model->getKey() }}]</h5>
@isset($prepend)
    {{ $prepend }}
@endisset
<x-smc::resource.part.create-edit-form
        type="update"
        method="PUT"
        :model="$model"
        :params="$params"
        :formActionMethod="$formActionMethod"
        :cancelActionMethod="$cancelActionMethod"
        :formView="$formView"
        :cancelController="$cancelController"
        :controller="$controller">{{ $slot }}</x-smc::resource.part.create-edit-form>
@isset($append)
    {{ $append }}
@endisset
