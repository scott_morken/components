<?php
/**
 * @var \Smorken\Domain\ViewModels\Contracts\RetrieveViewModel $viewModel
 * @var \Smorken\Model\Contracts\Model $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
use Illuminate\Support\HtmlString;
use Smorken\Components\Helpers\Action;
use Smorken\Support\Filter;

$model = $viewModel->model();
$filter = $viewModel->filter() ?? new Filter();
?>
@props([
    'viewModel',
    'title' => null,
    'limitAttributes' => [],
    'extra' => [],
    'controller' => $controller,
    'deleteActionMethod' => 'destroy',
    'cancelController' => $controller,
    'cancelActionMethod' => 'index',
])
@php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
@if($title)
    <x-smc::resource.title-back>{{ $title }}</x-smc::resource.title-back>
@endif
<h5 class="mb-2">Delete record [{{ $model->getKey() }}]</h5>
<div class="alert alert-danger">
    <div class="mb-2">Are you sure you want to delete this record?</div>
    @if ($slot->isEmpty())
        <x-smc::resource.part.show-attribute-rows :model="$model" :limitAttributes="$limitAttributes"
                                                  :extra="$extra"></x-smc::resource.part.show-attribute-rows>
    @else
        {{ $slot }}
    @endif
</div>
<x-smc::form method="DELETE"
             :action="new HtmlString(action(Action::make($controller, $deleteActionMethod), $params))">
    <div class="mb-2">
        <x-smc::button.danger type="submit">Delete</x-smc::button.danger>
        <x-smc::button.outline-secondary
                :href="new HtmlString(action(Action::make($cancelController, $cancelActionMethod), $params))">
            Cancel
        </x-smc::button.outline-secondary>
    </div>
</x-smc::form>
