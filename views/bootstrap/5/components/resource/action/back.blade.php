@php use Illuminate\Support\HtmlString;use Smorken\Components\Helpers\Action; @endphp
@props([
    'actionMethod' => 'index',
    'controller' => $controller,
    'params' => null,
    'filter' => null,
    'title' => 'Back',
])
<x-smc::link
        :href="new HtmlString(action(Action::make($controller, $actionMethod), $params ?? ($filter?->toArray() ?? [])))"
        :title="$title"></x-smc::link>
