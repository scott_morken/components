@php use Illuminate\Support\HtmlString;use Smorken\Components\Helpers\Action; @endphp
@props(['params' => [], 'actionMethod' => 'confirmDelete', 'controller' => $controller])
<x-smc::button.danger
        :href="new HtmlString(action(Action::make($controller, $actionMethod), $params))"
        {{ $attributes }}>{{ !$slot->isEmpty() ? $slot : 'Delete' }}</x-smc::button.danger>
