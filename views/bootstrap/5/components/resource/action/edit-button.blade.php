@php use Illuminate\Support\HtmlString;use Smorken\Components\Helpers\Action; @endphp
@props(['params' => [], 'actionMethod' => 'edit', 'controller' => $controller])
<x-smc::button.primary
        :href="new HtmlString(action(Action::make($controller, $actionMethod), $params))"
        {{ $attributes }}>{{ !$slot->isEmpty() ? $slot : 'Edit' }}</x-smc::button.primary>
