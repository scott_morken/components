@props([
    'params' => [],
    'editController' => $controller,
    'confirmDeleteController' => $controller,
    'editActionMethod' => 'edit',
    'confirmDeleteActionMethod' => 'confirmDelete',
    ])
<span {{ $attributes->class(['float-md-end']) }}>
<x-smc::resource.action.edit :params="$params" :controller="$editController" :action-method="$editActionMethod"
                             class="me-5"></x-smc::resource.action.edit>
<x-smc::resource.action.confirm-delete :params="$params" :controller="$confirmDeleteController"
                                       :action-method="$confirmDeleteActionMethod"></x-smc::resource.action.confirm-delete>
</span>
