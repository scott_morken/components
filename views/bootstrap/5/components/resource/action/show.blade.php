@php use Illuminate\Support\HtmlString;use Smorken\Components\Helpers\Action; @endphp
@props(['params' => [], 'actionMethod' => 'show', 'controller' => $controller])
<x-smc::link
        :href="new HtmlString(action(Action::make($controller, $actionMethod), $params))"
        title="Show {{ $slot }}">{{ $slot }}</x-smc::link>
