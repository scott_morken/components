@php use Illuminate\Support\HtmlString;use Smorken\Components\Helpers\Action; @endphp
@props(['params' => [], 'actionMethod' => 'confirmDelete', 'controller' => $controller])
<x-smc::link
        :href="new HtmlString(action(Action::make($controller, $actionMethod), $params))"
        {{ $attributes->class(['text-danger']) }}>{{ !$slot->isEmpty() ? $slot : 'delete' }}</x-smc::link>
