@php use Illuminate\Support\HtmlString;use Smorken\Components\Helpers\Action; @endphp
@props([
    'actionMethod' => 'create',
    'controller' => $controller,
    'params' => null,
    'filter' => null,
    'float' => true,
])
<div @class(['clearfix' => $float])>
    <x-smc::button.outline-success
            :href="new HtmlString(action(Action::make($controller, $actionMethod), $params ?? ($filter?->toArray() ?? [])))"
            {{ $attributes->class(['btn-sm', 'float-md-end' => $float]) }}
    >{{ !$slot->isEmpty() ? $slot : 'Create' }}</x-smc::button.outline-success>
</div>
