@php use Illuminate\Support\HtmlString;use Smorken\Components\Helpers\Action; @endphp
@props(['params' => [], 'actionMethod' => 'edit', 'controller' => $controller])
<x-smc::link
        :href="new HtmlString(action(Action::make($controller, $actionMethod), $params))"
        {{ $attributes->class(['text-success']) }}>{{ !$slot->isEmpty() ? $slot : 'edit' }}</x-smc::link>
