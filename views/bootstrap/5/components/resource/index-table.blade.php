<?php
use Illuminate\Support\Collection;
use Smorken\Components\Helpers\Model;
use Smorken\Domain\ViewModels\IterableViewModel;
use Smorken\Support\Filter;
use Smorken\Components\Helpers\ModelToArray;

/**
 * @var \Smorken\Domain\ViewModels\Contracts\IterableViewModel $viewModel
 * @var \Smorken\Support\Contracts\Filter|\Smorken\QueryStringFilter\Contracts\QueryStringFilter $filter
 */
$filter = $viewModel->filter() ?? new Filter();
?>
@props([
    'viewModel',
    'title' => null,
    'extra' => [],
    'limitAttributes' => [],
    'canCreate' => true,
    'viewColumn' => true,
    'actionsColumn' => true,
    'formFilterView' => null,
    'showController' => $controller,
    'showActionMethod' => 'show',
    'editController' => $controller,
    'editActionMethod' => 'edit',
    'confirmDeleteController' => $controller,
    'confirmDeleteActionMethod' => 'confirmDelete',
    'tableResponsive' => null,
])
@if($title)
    <x-smc::title>{{ $title }}</x-smc::title>
@endif
@if($canCreate)
    <x-smc::resource.action.create></x-smc::resource.action.create>
@endif
@isset($formFilter)
    {{ $formFilter }}
@endisset
@if ($formFilterView)
    @includeIf($formFilterView)
@endif
@isset($prepend)
    {{ $prepend }}
@endisset
@if ($viewModel->models() && count($viewModel->models()))
    @if ($tableResponsive)
        <div class="table-responsive-{{ $tableResponsive }}">
    @endif
            <x-smc::table>
                <x-slot:head>
                    @isset($head)
                        {{ $head }}
                    @else
                        @php($first = $viewModel->models()->first())
                        @foreach ($first->getAttributes() as $attribute => $v)
                            @if (is_iterable($v) && !is_a($v, \Stringable::class))
                                @continue
                            @endif
                            @if (empty($limitAttributes) || in_array($attribute, $limitAttributes))
                                <x-smc::table.heading>
                                    {{ method_exists($first, 'friendlyKey') ? $first->friendlyKey($attribute) : $attribute }}
                                </x-smc::table.heading>
                            @endif
                        @endforeach
                        @foreach ($extra as $attribute => $value)
                            <x-smc::table.heading>{{ $attribute }}</x-smc::table.heading>
                        @endforeach
                        @if ($actionsColumn)
                            <x-smc::table.heading>&nbsp;</x-smc::table.heading>
                        @endif
                    @endisset
                </x-slot:head>
                <x-slot:body>
                    @isset($body)
                        {{ $body }}
                    @else
                        @foreach ($viewModel->models() as $model)
                            @php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
                            <x-smc::table.row :model="$model">
                                @foreach (ModelToArray::from($model)->toArray() as $attribute => $value)
                                    @if (is_iterable($v) && !is_a($value, \Stringable::class))
                                        @continue
                                    @endif
                                    @if (empty($limitAttributes) || in_array($attribute, $limitAttributes))
                                        @if ($attribute === $model->getKeyName() && $viewColumn)
                                            <x-smc::table.cell>
                                                <x-smc::resource.action.show
                                                        title="View {{ $model->getKey() }}"
                                                        :controller="$showController"
                                                        :actionMethod="$showActionMethod"
                                                        :params="$params">{{ $model->getKey() }}</x-smc::resource.action.show>
                                            </x-smc::table.cell>
                                        @else
                                            <x-smc::table.cell
                                                    :model="Model::newInstance($model, $attribute)"></x-smc::table.cell>
                                        @endif
                                    @endif
                                @endforeach
                                @foreach ($extra as $attribute => $value)
                                    <x-smc::table.cell>
                                        {{ $value instanceof Closure ? $value($model) : \Smorken\Components\Helpers\Value::fromValue($value) }}
                                    </x-smc::table.cell>
                                @endforeach
                                @if ($actionsColumn)
                                    <x-smc::table.cell>
                                        <x-smc::resource.action.edit-confirm-delete
                                                :editController="$editController"
                                                :editActionMethod="$editActionMethod"
                                                :confirmDeleteController="$confirmDeleteController"
                                                :confirmDeleteActionMethod="$confirmDeleteActionMethod"
                                                :params="$params"></x-smc::resource.action.edit-confirm-delete>
                                    </x-smc::table.cell>
                                @endif
                            </x-smc::table.row>
                        @endforeach
                    @endisset
                </x-slot:body>
            </x-smc::table>
    @if ($tableResponsive)
        </div>
    @endif
    <x-smc::paginate.models :models="$viewModel->models()" :filter="$filter"></x-smc::paginate.models>
@else
    <div class="text-muted">No records found.</div>
@endif
@isset($append)
    {{ $append }}
@endisset
