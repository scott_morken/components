<?php
/**
 * @var \Smorken\Domain\ViewModels\Contracts\RetrieveViewModel $viewModel
 */
use Smorken\Support\Filter;

$model = $viewModel->model();
$filter = $viewModel->filter() ?? new Filter();
?>
@props([
    'viewModel',
    'title' => null,
    'formActionMethod' => 'store',
    'cancelController' => $controller,
    'cancelActionMethod' => 'index',
    'formView' => null,
])
@php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
@if($title)
    <x-smc::resource.title-back>{{ $title }}</x-smc::resource.title-back>
@endif
<h5 class="mb-2">Create new record</h5>
@isset($prepend)
    {{ $prepend }}
@endisset
<x-smc::resource.part.create-edit-form
        type="create"
        :model="$model"
        :params="$params"
        :formActionMethod="$formActionMethod"
        :cancelActionMethod="$cancelActionMethod"
        :formView="$formView"
        :cancelController="$cancelController"
        :controller="$controller">{{ $slot }}</x-smc::resource.part.create-edit-form>
@isset($append)
    {{ $append }}
@endisset
