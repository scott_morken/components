<?php
use Smorken\Components\Helpers\Value;
use Smorken\Components\Helpers\ModelToArray;

/**
 * @var \Smorken\Model\Contracts\Model $model
 */
?>
@props([
    'model',
    'limitAttributes' => [],
    'firstCol' => 'col-sm-3 col-md-2',
    'secondCol' => 'col-sm-9 col-md-10',
    'extra' => [],
])
@foreach (ModelToArray::from($model)->toArray() as $attribute => $value)
    @if (is_iterable($value) && !is_a($value, \Stringable::class))
        @continue
    @endif
    @if (empty($limitAttributes) || in_array($attribute, $limitAttributes))
        <x-smc::resource.part.show-row :model="$model" :attribute="$attribute"
                                       :value="$value"
                                       :firstCol="$firstCol"
                                       :secondCol="$secondCol"></x-smc::resource.part.show-row>
    @endif
@endforeach
@foreach ($extra as $attribute => $value)
    <x-smc::resource.part.show-row :model="$model" :attribute="$attribute"
                                   :value="$value instanceof Closure ? $value($model) : Value::fromValue($value, true)"
                                   :firstCol="$firstCol"
                                   :secondCol="$secondCol"></x-smc::resource.part.show-row>
@endforeach
