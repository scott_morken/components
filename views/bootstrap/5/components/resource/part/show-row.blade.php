@php use Smorken\Support\Arr; @endphp
@props([
    'attribute',
    'value' => null,
    'model' => null,
    'key' => null,
    'firstCol' => 'col-sm-3 col-md-2',
    'secondCol' => 'col-sm-9 col-md-10',
])
<x-smc::row.key-value-row
        :key="$key ?? ($model && method_exists($model, 'friendlyKey') ? $model->friendlyKey($attribute) : $attribute)"
        :value="$value"
        :firstCol="$firstCol"
        :secondCol="$secondCol"
>{{ $slot }}</x-smc::row.key-value-row>
