@php use Illuminate\Support\HtmlString;use Smorken\Components\Helpers\Action; @endphp
@props([
    'formActionMethod',
    'type',
    'cancelController' => $controller,
    'cancelActionMethod' => 'index',
    'controller' => $controller,
    'method' => 'POST',
    'model' => null,
    'formView' => null,
    'params' => [],
])
<x-smc::form
        :action="new HtmlString(action(Action::make($controller, $formActionMethod), $params))"
        :method="$method"
        :model="$model">
    @if ($formView)
        @includeIf($formView)
    @endif
    {{ $slot }}
    <div class="mt-4 mb-2">
        <x-smc::button.primary type="submit">Save</x-smc::button.primary>
        <x-smc::button.outline-secondary
                :href="new HtmlString(action(Action::make($cancelController, $cancelActionMethod), $params))">
            Cancel
        </x-smc::button.outline-secondary>
    </div>
</x-smc::form>
