@if (isset($errors) && $errors->any())
    <div {{ $attributes->class(['clear-alert', 'errors', 'p-2', 'mb-2', 'text-bg-danger']) }} role="alert">
        <div class="container">
            <div class="fw-bold">{{ __('Errors were found.') }}</div>
            @foreach($errors->all() as $error)
                <div class="p-2">{{ $error }}</div>
            @endforeach
        </div>
    </div>
@endif
