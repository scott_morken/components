<?php
use Smorken\Components\Helpers\Value;

?>
<td {{ $attributes }}>{{ $slot->isEmpty() ? Value::fromValue($getValue(false), true)->htmlable() : $slot }}</td>
