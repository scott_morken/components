@props(['striped' => true])
<table {{ $attributes->class(['table', 'table-striped' => $striped]) }}>
    @isset($head)
        <thead>
        <tr {{ $head->attributes }}>
            {{ $head }}
        </tr>
        </thead>
    @endisset
    <tbody {{ $body->attributes }}>
    {{ $body }}
    </tbody>
    @isset($footer)
        <tfoot {{ $footer->attributes }}>{{ $footer }}</tfoot>
    @endisset
</table>
