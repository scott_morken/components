<tr {{ $attributes }}
    @if($hasId()) id="{{ $getId() }}" @endif
>
    {{ $slot }}
</tr>
