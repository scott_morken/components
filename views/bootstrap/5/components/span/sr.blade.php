<span {{ $attributes->class(['visually-hidden']) }}>{{ $slot }}</span>
