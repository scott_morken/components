<x-smc::icon :attributes="$attributes->class(['bi-circle-fill'])">
    <circle cx="8" cy="8" r="8"/>
</x-smc::icon>
