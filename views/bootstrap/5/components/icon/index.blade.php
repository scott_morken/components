<svg {{ $attributes->merge(['class' => 'bi', 'width' => '1em', 'height' => '1em', 'fill' => 'currentColor', 'viewBox' => '0 0 16 16']) }}
     xmlns="http://www.w3.org/2000/svg">
    {{ $slot }}
</svg>
