@props(['container' => 'container', 'homeUrl' => url('/')])
<nav class="navbar navbar-expand-md navbar-dark shadow-sm bg-brand">
    <div class="{{ $container }}">
        <a class="navbar-brand" href="{{ $homeUrl }}">
            <img src="{{ asset('images/logo.png') }}" alt="Logo">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav me-auto">
                @guest
                    <x-smc::menu.by-key key="guest"/>
                @else
                    <x-smc::menu.by-key key="auth"/>
                @endguest
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav justify-content-right">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                @else
                    <x-smc::menu.by-role role="role-manage" title="Manage"/>
                    <x-smc::menu.by-role role="role-admin" title="Admin"/>
                    <li class="nav-item dropdown" id="logout-container">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        >{{ auth()->user()->shortName }} <span class="caret"></span></a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <x-smc::form.submit action="{{ route('logout') }}"
                                                class="btn btn-link dropdown-item"
                                                formId="logout-form">{{ __('Logout') }}</x-smc::form.submit>
                            @if(\Illuminate\Support\Facades\Route::has('logout.endpoint'))
                                <x-smc::form.submit action="{{ route('logout.endpoint') }}"
                                                    class="btn btn-link dropdown-item"
                                                    formId="logout-all-form">{{ __('Logout Everywhere') }}</x-smc::form.submit>
                            @endif
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
<x-smc::flash.messages/>
<x-smc::errors/>
<div id="messages"></div>
<x-smc::menu.submenu/>
