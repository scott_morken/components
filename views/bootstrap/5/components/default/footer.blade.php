<div class="text-center">
    <div class="text-muted"><small>&copy; {{ date('Y') }} Phoenix College</small>
        @if (session()->has('impersonating'))
            <span class="text-danger">&middot; Impersonating [{{ session()->get('impersonating') }}]</span>
        @endif
        <div>
            <a href="https://www.maricopa.edu" target="_blank" title="Maricopa Community Colleges" rel="noopener">
                <img src="{{ asset('images/footer.png') }}" alt="A Maricopa Community College"
                     height="30">
            </a>
        </div>
    </div>
</div>
