@props(['container' => 'container'])
<nav class="navbar navbar-expand-md navbar-dark shadow-sm bg-brand">
    <div class="{{ $container }}">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('images/logo.png') }}" alt="Logo">
            {{ config('app.name', 'Laravel') }}
        </a>
    </div>
</nav>
<x-smc::flash.messages></x-smc::flash.messages>
<x-smc::errors></x-smc::errors>
<div id="messages"></div>
