<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(isset($title) && $title)
        <title>{{ e($title) }}</title>
    @else
        <title>{{ config('app.name', 'Laravel') }}</title>
    @endif
    @if(isset($favicon) && $favicon)
        {{ $favicon }}
    @else
        <link rel="icon" href="{{ asset('favicon.ico') }}" sizes="16x16 32x32 48x48 64x64"
              type="image/vnd.microsoft.icon">
    @endif
    <!-- Scripts -->
    @if(isset($scriptsHead) && $scriptsHead)
        {{ $scriptsHead }}
    @endif
    <!-- Styles -->
    @if(isset($styles) && $styles)
        {{ $styles }}
    @else
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @endif
    @stack('add_to_head')
</head>
<body>
<div id="app">
    {{ $slot }}
</div>
@if(isset($scriptsEnd) && $scriptsEnd)
    {{ $scriptsEnd }}
@else
    <script src="{{ asset('js/app.js') }}"></script>
@endif
@stack('add_to_end')
</body>
</html>
