@props([
    'isFluid' => false,
])
<x-smc::layouts.base :title="$title ?? null" :favicon="$favicon ?? null" :scriptsHead="$scriptsHead ?? null"
                     :styles="$styles ?? null" :scriptsEnd="$scriptsEnd ?? null">
    <div id="ajax-loading" class="spinner-border text-warning" role="status"><span
                class="visually-hidden">Loading...</span></div>
    <header>
        @if (isset($header) && !$header->isEmpty())
            {{ $header }}
        @else
            <x-smc::default.header-no-nav :container="$isFluid ? 'container-fluid' : 'container'"></x-smc::default.header-no-nav>
        @endif
    </header>
    <main {{ $attributes->class(['py-4', ($isFluid ? 'container-fluid' : 'container')]) }}>
        {{ $slot }}
    </main>
    <footer {{ $attributes->class(['py-4', ($isFluid ? 'container-fluid' : 'container')])->only(['class']) }}>
        @if (isset($footer) && !$footer->isEmpty())
            {{ $footer }}
        @else
            <x-smc::default.footer></x-smc::default.footer>
        @endif
    </footer>
</x-smc::layouts.base>
