@aware(['containerId', 'show' => false])
<div {{ $attributes->class(['collapse', 'show' => $show])->except('id') }} id="{{ $containerId }}">{{ $slot }}</div>
