@props(['link' => true, 'href' => null])
@aware(['containerId', 'show' => false])
@php
    $defaults = [
        'data-bs-toggle' => 'collapse',
        'aria-expanded' => $show ? 'true' : 'false',
        'aria-controls' => $containerId,
        'data-bs-target' => '#'.$containerId,
    ];
    if ($link):
        $defaults['role'] = 'button';
        $defaults['href'] = $href ?? '#'.$containerId;
    endif;
@endphp
@if ($link)
    <x-smc::link {{ $attributes->merge($defaults) }}>{{ $slot }}</x-smc::link>
@else
    <x-smc::button {{ $attributes->merge($defaults) }}>{{ $slot }}</x-smc::button>
@endif
