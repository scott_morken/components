@props(['containerId', 'show' => false])
<div {{ $attributes }}>
    <x-smc::collapse.action :attributes="$action->attributes">{{ $action }}</x-smc::collapse.action>
    @isset($container)
        <x-smc::collapse.container :attributes="$container->attributes">{{ $container }}</x-smc::collapse.container>
    @else
        <x-smc::collapse.container>{{ $slot }}</x-smc::collapse.container>
    @endisset
</div>
