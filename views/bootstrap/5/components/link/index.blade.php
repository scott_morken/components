@props(['href' => '#', 'title' => null])
<?php $title = $title ?? ($slot->isNotEmpty() ? e($slot) : $href); ?>
<a href="{{ new \Illuminate\Support\HtmlString($href ?? '') }}"
   title="{{ $title }}" {{ $attributes }}>{{ $slot->isNotEmpty() ? $slot : $title }}</a>
