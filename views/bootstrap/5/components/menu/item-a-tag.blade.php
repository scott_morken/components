@props(['menuItem', 'active'])
<a {{ $attributes->class(['active' => $active]) }}
   aria-current="{{ $active ? 'true' : false }}"
   href="{{ new \Illuminate\Support\HtmlString(action($menuItem->action)) }}"
>{{ $menuItem->name }}</a>
