@props(['key'])
@php $items = \Smorken\Menu\Facades\Menu::getMenusByKey($key); @endphp
<x-smc::menu.li-items :items="$items"></x-smc::menu.li-items>

