@props(['items', 'submenu' => null])
@if ($items)
    @foreach ($items as $menuItem)
        @php
            $active = \Smorken\Menu\Facades\Menu::isActiveChain($controller ?? null, $menuItem);
            $submenu = ($submenu === null && $active && count($menuItem->children)) ? $menuItem->children : $submenu;
        @endphp
        @if ($menuItem->visible)
            <li class="nav-item">
                <x-smc::menu.item-a-tag class="nav-link" :menu-item="$menuItem"
                                        :active="$active"></x-smc::menu.item-a-tag>
            </li>
        @endif
    @endforeach
    @if ($submenu !== null)
        @php \Smorken\Menu\Facades\Menu::setSubMenus($submenu) @endphp
    @endif
@endif
