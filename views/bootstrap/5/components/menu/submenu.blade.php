@props(['subMenu' => null])
@php
    $subMenu = $subMenu ?? \Smorken\Menu\Facades\Menu::getSubMenus();
    $next = null;
@endphp
@if ($subMenu)
    <nav class="container sub-menu my-1">
        <ul class="nav nav-pills">
            @foreach ($subMenu as $menuItem)
                    <?php $active = \Smorken\Menu\Facades\Menu::isActiveChain($controller ?? null, $menuItem); ?>
                @if ($active && count($menuItem->children))
                    @php $next = $menuItem->children; @endphp
                @endif
                <x-smc::menu.item-a-tag class="nav-link" :menu-item="$menuItem" :active="$active"></x-smc::menu.item-a-tag>
            @endforeach
        </ul>
    </nav>
@endif
@if ($next)
    <x-smc::menu.submenu :subMenu="$next"></x-smc::menu.submenu>
@endif
