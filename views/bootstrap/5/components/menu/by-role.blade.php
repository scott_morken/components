@props(['role', 'title' => 'Admin'])
@can($role)
    @php $menus = \Smorken\Menu\Facades\Menu::getMenusByKey($role); @endphp
    @if ($menus)
        @php
            $submenu = null;
            $id = sprintf('navbar-%s-dropdown', \Illuminate\Support\Str::slug($role));
            $activeDropdown = \Smorken\Menu\Facades\Menu::isActiveChain($controller ?? null, $menus);
        @endphp
        <li class="nav-item dropdown">
            <a id="{{ $id }}"
               class="nav-link dropdown-toggle {{  $activeDropdown ? 'active' : null }}"
               href="#" role="button" data-bs-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                {{ $title }} <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="{{ $id }}">
                @foreach ($menus as $menu)
                    @php
                        $active = \Smorken\Menu\Facades\Menu::isActiveChain($controller ?? null, $menu);
                        $submenu = ($submenu === null && $active && count($menu->children)) ? $menu->children : $submenu;
                    @endphp
                    @if ($menu->visible)
                        <li>
                            <x-smc::menu.item-a-tag :menu-item="$menu" :active="$active" class="dropdown-item"/>
                        </li>
                    @endif
                @endforeach
            </ul>
        </li>
        @if ($submenu)
            @php \Smorken\Menu\Facades\Menu::setSubMenus($submenu); @endphp
        @endif
    @endif
@endcan
