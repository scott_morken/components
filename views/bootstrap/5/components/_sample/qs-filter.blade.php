<?php
/**
 * @var \Smorken\Components\Contracts\Helpers\FormQsFilter $helper
 */
?>
<x-smc::form.qs-filter>
    @php($helper = $component->helper)
    <x-smc::flex class="justify-content-start">
        <x-smc::input.group>
            <x-smc::input.label for="filter-name" :visible="false">Name</x-smc::input.label>
            <x-smc::input name="filter[name]" class="{{ $helper->getClasses('name') }}"
                          :value="$helper->getValue('name')" placeholder="Name..."/>
        </x-smc::input.group>
        <x-smc::input.group>
            <x-smc::input.label for="filter-active" :visible="false">Status</x-smc::input.label>
            <x-smc::input.select-options name="filter[active]" class="{{ $helper->getClasses('active') }}"
                                         :value="$helper->getValue('active')"
                                         :options="['' => '-- Any Status --', '1' => 'Active', '0' => 'Inactive']"
            />
        </x-smc::input.group>
        <x-smc::form.filter-buttons></x-smc::form.filter-buttons>
    </x-smc::flex>
</x-smc::form.qs-filter>
