<x-layouts.app>
    <x-smc::preset.view-rows title="Sample Administration"
                               :filter="$filter"
                               :model="$model"></x-smc::preset.view-rows>
</x-layouts.app>
