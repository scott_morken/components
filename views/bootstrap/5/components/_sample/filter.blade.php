<x-smc::form.filter>
    @php($helper = $component->helper)
    <x-smc::flex class="justify-content-start">
        <x-smc::input.group>
            <x-smc::input.label for="f_name" :visible="false">Name</x-smc::input.label>
            <x-smc::input name="f_name" class="{{ $helper->getClasses('f_name') }}"
                          :value="$helper->getValue('f_name')" placeholder="Name..."/>
        </x-smc::input.group>
        <x-smc::input.group>
            <x-smc::input.label for="f_active" :visible="false">Status</x-smc::input.label>
            <x-smc::input.select-options name="f_active" class="{{ $helper->getClasses('f_active') }}"
                                         :value="$helper->getValue('f_active')"
                                         :options="['' => '-- Any Status --', '1' => 'Active', '0' => 'Inactive']"
            />
        </x-smc::input.group>
        <x-smc::form.filter-buttons></x-smc::form.filter-buttons>
    </x-smc::flex>
</x-smc::form.filter>
