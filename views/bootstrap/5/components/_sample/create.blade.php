<x-layouts.app>
    <x-smc::preset.create title="Sample Administration"
                          form-view="admin.sample._form"
                          :model="$model"
                          :filter="$filter">
        <x-smc::input id="test"></x-smc::input>
    </x-smc::preset.create>
</x-layouts.app>
