<x-layouts.app>
    <x-smc::preset.delete title="Sample Administration"
                               :filter="$filter"
                               :model="$model"></x-smc::preset.delete>
</x-layouts.app>
