<x-layouts.app>
    <x-smc::preset.index-table title="Sample Administration"
                               :filter="$filter"
                               :models="$models"></x-smc::preset.index-table>
</x-layouts.app>
