<?php

use Smorken\Components\Helpers\Model;

/**
 * @var \Smorken\Model\Contracts\Model $model
 */
?>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'description'))
    <x-smc::input.label :model="$m">Description</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'request_type'))
    <x-smc::input.label :model="$m">Request Type</x-smc::input.label>
    <x-smc::input.select-options
            :model="$m"
            :options="$requestTypes"></x-smc::input.select-options>
</x-smc::input.group>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'active'))
    <x-smc::input.checkbox :model="$m"
                           :checkable="new \Smorken\Components\Helpers\Checkbox('1', 'Active', null, false, '0')"></x-smc::input.checkbox>
</x-smc::input.group>
