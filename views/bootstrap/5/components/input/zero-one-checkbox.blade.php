@props(['label' => 'Active', 'model' => null, 'name' => null, 'id' => null, 'withErrors' => true])
<x-smc::input.group>
    @php($checkable = new \Smorken\Components\Helpers\Checkbox('1', $label, null, false, '0'))
    <x-smc::input.checkbox :model="$model" :checkable="$checkable" :name="$name" :id="$id"
                           :withErrors="$withErrors"></x-smc::input.checkbox>
</x-smc::input.group>
