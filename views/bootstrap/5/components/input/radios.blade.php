<?php
/**
 * @property \Smorken\Components\Contracts\Helpers\Checkable $radio
 */
?>
<div {{ $attributes->class(['radios']) }}>
    @foreach($radios as $radio)
        <x-smc::input.radio :model="$model" :checkable="$radio"></x-smc::input.radio>
    @endforeach
    @if($withErrors)
        <x-smc::input.error :for="$getName()"/>
    @endif
</div>
