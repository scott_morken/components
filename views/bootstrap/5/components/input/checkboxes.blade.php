<?php
/**
 * @property \Smorken\Components\Contracts\Helpers\Checkbox $checkbox
 */
?>
<div {{ $attributes->class(['checkboxes']) }}>
    @foreach($checkboxes as $checkbox)
        <x-smc::input.checkbox :model="$model" :checkable="$checkbox"></x-smc::input.checkbox>
    @endforeach
    @if($withErrors)
        <x-smc::input.error :for="$getName()"/>
    @endif
</div>
