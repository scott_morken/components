@props(['for', 'tag' => 'div'])
@isset($errors)
    @if($for)
        @error($for)
        <{{ $tag }} {{ $attributes->class(['text-danger', 'small']) }}>{{ $message }}</{{ $tag }}>
        @enderror
    @endif
@endif
