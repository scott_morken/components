@props(['bottomMargin' => true])
<div {{ $attributes->class(['mb-2' => $bottomMargin]) }}>
    {{ $slot }}
</div>
