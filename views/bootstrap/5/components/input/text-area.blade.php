<textarea {{ $attributes->class(['form-control', 'is-invalid' => $hasErrors()])->merge(['rows' => '3', 'maxlength' => '4096']) }}
>{{ $getValue() ?: $slot }}</textarea>
@if($withErrors)
    <x-smc::input.error :for="$getName()"/>
@endif
