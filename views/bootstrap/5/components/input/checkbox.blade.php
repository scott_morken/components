<?php
/**
 * @var \Smorken\Components\Contracts\Helpers\Checkbox $checkable
 */
?>
<div class="form-check {{ $attributes->has('inline') ? 'form-check-inline' : null }}">
    @if ($hasHiddenValue())
        <input type="hidden" {{ $attributes->only(['name']) }} @disabled($disabled)
        value="{{ $checkable->hiddenValue }}"/>
    @endif
    <input type="checkbox"
           {{ $attributes->class(['form-check-input', 'is-invalid' => $hasErrors()])->except(['value']) }}
           value="{{ $checkable->value }}"
            @checked($isChecked($getValue()))
            @readonly($readonly)
            @disabled($disabled)
    />
    @if ($hasLabel())
        <label class="form-check-label" for="{{ $getId() }}">{{ $checkable->label }}</label>
    @endif
    {{ $slot ?? null }}
    @if (!$isMultiple() && $withErrors)
        <x-smc::input.error tag="span" :for="$getName()"/>
    @endif
</div>
