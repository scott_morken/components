<?php
/**
 * @var \Smorken\Components\Contracts\Helpers\Checkable $checkable
 */
?>
<div class="form-check {{ $attributes->has('inline') ? 'form-check-inline' : null }}">
    <input type="radio"
           {{ $attributes->class(['form-check-input', 'is-invalid' => $hasErrors()])->except(['value']) }}
           value="{{ $checkable->value }}"
            @checked($isChecked($getValue()))
            @disabled($disabled)
            @readonly($readonly)
    />
    @if ($hasLabel())
        <label class="form-check-label" for="{{ $getId() }}">{{ $checkable->label }}</label>
    @endif
    {{ $slot ?? null }}
    @if($withErrors)
        <x-smc::input.error tag="span" :for="$getName()"/>
    @endif
</div>
