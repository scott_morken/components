<?php
/**
 * @var \Smorken\Components\Contracts\Helpers\Option $option
 */
?>
<select {{ $attributes->class(['form-select', 'is-invalid' => $hasErrors()])->except(['value']) }}
        @disabled($disabled)
>
    @foreach ($options as $option)
        <option value="{{ $option->value }}"
                @selected($option->isSelected($getValue()))
                @disabled($option->disabled)
        >{{ $option->text }}</option>
    @endforeach
</select>
{{ $slot ?? null }}
@if($withErrors)
    <x-smc::input.error :for="$getName()"/>
@endif
