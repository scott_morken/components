<input type="{{ $type ?? 'text' }}"
       @disabled($disabled)
       @readonly($readonly)
       {{ $attributes->class(['form-control', 'is-invalid' => $hasErrors()])->merge(['maxlength' => 255])->except(['value']) }}
       value="{{ $getValue(true, true) }}"
/>
{{ $slot ?? null }}
@if($withErrors)
    <x-smc::input.error :for="$getName()"/>
@endif
