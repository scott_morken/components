<select {{ $attributes->class(['form-select', 'is-invalid' => $hasErrors()])->except(['value']) }}
        @disabled($disabled)
>
    {{ $slot }}
</select>
@if($withErrors)
    <x-smc::input.error :for="$attributes['id'] ?? null"/>
@endif
