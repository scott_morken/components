<label for="{{ $for ?: ($hasId() ? $getId() : null) }}"
        {{ $attributes->class(['form-label', 'visually-hidden' => !$visible])->except(['name', 'id']) }}
>{{ $slot }}</label>
