<form {{ $attributes->merge($additionalAttributes()) }}>
    @if ($wantsCsrf())
        @csrf
    @endif
    @if ($shouldSpoof())
        @method($method)
    @endif
    {{ $slot }}
</form>
