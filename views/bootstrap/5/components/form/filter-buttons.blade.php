@props([
        'showFilter' => true,
        'controller' => $controller ?? null,
        'resetMethod' => 'index',
        'resetParams' => [],
        'reset' => true,
])
<div {{ $attributes }}>
    @if ($showFilter)
        <x-smc::button.primary type="submit" @class(['me-2' => $reset])>Filter</x-smc::button.primary>
    @endif
    @if ($controller && $reset)
        <x-smc::link :href="action(\Smorken\Components\Helpers\Action::make($controller, $resetMethod), $resetParams)"
                     class="btn btn-outline-danger"
                     title="Reset filters">
            Reset
        </x-smc::link>
    @endif
</div>
