<x-smc::card class="my-2">
    <x-smc::card.body>
        <x-smc::form method="GET">
            {{ $slot }}
        </x-smc::form>
    </x-smc::card.body>
</x-smc::card>
