@props(['id' => 'modal', 'hidden' => true])
<div id="{{ $id }}" {{ $attributes->class(['modal', 'fade']) }} role="dialog"
     aria-labelledby="{{ $id }}-title" aria-hidden="{{ $hidden ? 'true' : 'false' }}">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="{{ $id }}-title">
                    @isset($title)
                        {{ $title }}
                    @endisset
                </h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="{{ $id }}-body">
                {{ $slot }}
            </div>
            <div class="modal-footer">
                @isset($footer)
                    {{ $footer }}
                @else
                    <x-smc::button.outline-dark class="btn-block w-100" data-bs-dismiss="modal">Close
                    </x-smc::button.outline-dark>
                @endif
            </div>
        </div>
    </div>
</div>
