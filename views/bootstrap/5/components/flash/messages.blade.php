@foreach (session()->all() as $key => $value)
    @if (str_starts_with($key, 'flash:'))
        <x-smc::flash.message :key="explode(':', substr($key, 6))[0] ?? ''" :value="$value"></x-smc::flash.message>
    @endif
@endforeach
