@php use Smorken\Support\Arr; @endphp
@props([
    'value',
    'key',
    'firstCol' => 'col-sm-3 col-md-2',
    'secondCol' => 'col-sm-9 col-md-10',
])
<div {{ $attributes->class(['row', 'mb-2']) }}>
    <div class="{{ $firstCol }} fw-bold">{{ $key }}</div>
    <div class="{{ $secondCol }}">
        {{ \Smorken\Components\Helpers\Value::fromValue($value, true)->htmlable() }}
        {{ $slot }}
    </div>
</div>
