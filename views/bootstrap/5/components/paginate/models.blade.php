@if ($isPaginated())
    <div {{ $attributes }}>
        {{ $getPaginator()->appends($getAppends())->links() }}
    </div>
@endif

