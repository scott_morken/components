@props([
    'formAction',
    'type',
    'cancelController' => $controller,
    'cancelAction' => 'index',
    'controller' => $controller,
    'model' => null,
    'formView' => null,
    'params' => [],
])
<x-smc::form :action="new \Illuminate\Support\HtmlString(action(\Smorken\Components\Helpers\Action::make($controller, $formAction), $params))" :model="$model">
    @if ($formView)
        @includeIf($formView)
    @endif
    {{ $slot }}
    <div class="mt-4 mb-2">
        <x-smc::button.primary type="submit">Save</x-smc::button.primary>
        <x-smc::button.outline-secondary :href="new \Illuminate\Support\HtmlString(action(\Smorken\Components\Helpers\Action::make($cancelController, $cancelAction), $params))">Cancel
        </x-smc::button.outline-secondary>
    </div>
</x-smc::form>
