<?php
use Smorken\Components\Helpers\Value;
?>
@props([
    'attribute',
    'value' => null,
    'model' => null,
    'firstCol' => 'col-sm-3 col-md-2',
    'secondCol' => 'col-sm-9 col-md-10',
])
<div {{ $attributes->class(['row', 'mb-2']) }}>
    <div class="{{ $firstCol }} fw-bold">
        {{ $model && method_exists($model, 'friendlyKey') ? $model->friendlyKey($attribute) : $attribute }}
    </div>
    <div class="{{ $secondCol }}">
        {{ Value::fromValue($value)->htmlable() }}
        {{ $slot }}
    </div>
</div>
