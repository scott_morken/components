@props([
    'actionMethod' => 'index',
    'controller' => $controller,
    'params' => null,
    'filter' => null,
    'title' => 'Back',
])
<x-smc::link :href="new \Illuminate\Support\HtmlString(action(\Smorken\Components\Helpers\Action::make($controller, $actionMethod), $params ?? ($filter?->toArray() ?? [])))"
             :title="$title"></x-smc::link>
