@props(['params' => [], 'actionMethod' => 'update', 'controller' => $controller])
<x-smc::link
        :href="new \Illuminate\Support\HtmlString(action(\Smorken\Components\Helpers\Action::make($controller, $actionMethod), $params))"
        {{ $attributes->class(['text-success']) }}>{{ !$slot->isEmpty() ? $slot : 'update' }}</x-smc::link>
