@props(['params' => [], 'actionMethod' => 'update', 'controller' => $controller])
<x-smc::button.primary
        :href="new \Illuminate\Support\HtmlString(action(\Smorken\Components\Helpers\Action::make($controller, $actionMethod), $params))"
        {{ $attributes }}>{{ !$slot->isEmpty() ? $slot : 'Update' }}</x-smc::button.primary>
