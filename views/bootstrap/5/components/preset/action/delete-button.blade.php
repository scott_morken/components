@props(['params' => [], 'actionMethod' => 'delete', 'controller' => $controller])
<x-smc::button.danger
        :href="new \Illuminate\Support\HtmlString(action(\Smorken\Components\Helpers\Action::make($controller, $actionMethod), $params))"
        {{ $attributes }}>{{ !$slot->isEmpty() ? $slot : 'Delete' }}</x-smc::button.danger>
