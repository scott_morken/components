@props(['params' => [], 'actionMethod' => 'view', 'controller' => $controller])
<x-smc::link :href="new \Illuminate\Support\HtmlString(action(\Smorken\Components\Helpers\Action::make($controller, $actionMethod), $params))" title="View {{ $slot }}">{{ $slot }}</x-smc::link>
