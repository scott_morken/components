@props(['params' => [], 'controller' => $controller, 'updateAction' => 'update', 'deleteAction' => 'delete'])
<span {{ $attributes->class(['float-md-end']) }}>
<x-smc::preset.action.update :params="$params" :controller="$controller" :action-method="$updateAction"
                             class="me-5"></x-smc::preset.action.update>
<x-smc::preset.action.delete :params="$params" :controller="$controller" :action-method="$deleteAction"></x-smc::preset.action.delete>
</span>
