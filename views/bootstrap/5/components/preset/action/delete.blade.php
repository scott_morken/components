@props(['params' => [], 'actionMethod' => 'delete', 'controller' => $controller])
<x-smc::link
        :href="new \Illuminate\Support\HtmlString(action(\Smorken\Components\Helpers\Action::make($controller, $actionMethod), $params))"
        {{ $attributes->class(['text-danger']) }}>{{ !$slot->isEmpty() ? $slot : 'delete' }}</x-smc::link>
