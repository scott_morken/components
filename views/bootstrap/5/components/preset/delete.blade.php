<?php
/**
 * @var \Smorken\Model\Contracts\Model $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@props([
    'model',
    'title' => null,
    'filter' => new \Smorken\Support\Filter(),
    'limitAttributes' => [],
    'extra' => [],
    'controller' => $controller,
    'deleteAction' => 'doDelete',
    'cancelController' => $controller,
    'cancelAction' => 'index',
])
@php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
@if($title)
    <x-smc::preset.title-back>{{ $title }}</x-smc::preset.title-back>
@endif
<h5 class="mb-2">Delete record [{{ $model->getKey() }}]</h5>
<div class="alert alert-danger">
    <div class="mb-2">Are you sure you want to delete this record?</div>
    @if ($slot->isEmpty())
        <x-smc::preset.view-attribute-rows :model="$model" :limitAttributes="$limitAttributes"
                                           :extra="$extra"></x-smc::preset.view-attribute-rows>
    @else
        {{ $slot }}
    @endif
</div>
<x-smc::form method="DELETE" :action="new \Illuminate\Support\HtmlString(action(\Smorken\Components\Helpers\Action::make($controller, $deleteAction), $params))">
    <div class="mb-2">
        <x-smc::button.danger type="submit">Delete</x-smc::button.danger>
        <x-smc::button.outline-secondary :href="new \Illuminate\Support\HtmlString(action(\Smorken\Components\Helpers\Action::make($cancelController, $cancelAction), $params))">Cancel
        </x-smc::button.outline-secondary>
    </div>
</x-smc::form>
