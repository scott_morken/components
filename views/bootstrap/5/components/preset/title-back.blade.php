@props([
    'backTitle' => 'Back',
    'actionMethod' => 'index',
    'controller' => $controller,
    'params' => null,
    'filter' => null,
    ])
<x-smc::flex.between {{ $attributes }}>
    <div>
        <x-smc::title>{{ $slot }}</x-smc::title>
    </div>
    <div>
        <x-smc::preset.action.back
                :title="$backTitle"
                :controller="$controller"
                :actionMethod="$actionMethod"
                :params="$params"
                :filter="$filter"
        ></x-smc::preset.action.back>
    </div>
</x-smc::flex.between>
