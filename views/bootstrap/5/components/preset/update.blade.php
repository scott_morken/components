@props([
    'model',
    'title' => null,
    'filter' => new \Smorken\Support\Filter(),
    'formAction' => 'doUpdate',
    'cancelAction' => 'index',
    'formView' => null,
])
@php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
@if($title)
    <x-smc::preset.title-back>{{ $title }}</x-smc::preset.title-back>
@endif
<h5 class="mb-2">Update record [{{ $model->getKey() }}]</h5>
@isset($prepend)
    {{ $prepend }}
@endisset
<x-smc::preset.create-update-form
        type="update"
        :model="$model"
        :params="$params"
        :formAction="$formAction"
        :cancelAction="$cancelAction"
        :formView="$formView"
        :controller="$controller">{{ $slot }}</x-smc::preset.create-update-form>
@isset($append)
    {{ $append }}
@endisset
