<?php
/**
 * @var \Smorken\Model\Contracts\Model $models
 * @var \Smorken\Support\Contracts\Filter|\Smorken\QueryStringFilter\Contracts\QueryStringFilter $filter
 */
?>
@props([
    'title' => null,
    'model' => null,
    'filter' => new \Smorken\Support\Filter(),
    'extra' => [],
    'limitAttributes' => [],
    'actions' => true,
    'backActionMethod' => 'index',
    'firstCol' => 'col-sm-3 col-md-2',
    'secondCol' => 'col-sm-9 col-md-10',
])
@php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
@if($title)
    <x-smc::preset.title-back :actionMethod="$backActionMethod">{{ $title }}</x-smc::preset.title-back>
@endif
<h5 class="mb-2">View record [{{ $model->getKey() }}]</h5>
@if ($actions)
    <div class="row mb-4">
        <div class="col">
            <x-smc::preset.action.update-button class="btn-block w-100"
                                                :params="$params"></x-smc::preset.action.update-button>
        </div>
        <div class="col">
            <x-smc::preset.action.delete-button class="btn-block w-100"
                                                :params="$params"></x-smc::preset.action.delete-button>
        </div>
    </div>
@endif
@isset($prepend)
    {{ $prepend }}
@endisset
@if ($slot->isEmpty())
    <x-smc::preset.view-attribute-rows :model="$model" :limitAttributes="$limitAttributes" :extra="$extra"
                                       :firstCol="$firstCol"
                                       :secondCol="$secondCol"></x-smc::preset.view-attribute-rows>
@else
    {{ $slot }}
@endif
@isset($append)
    {{ $append }}
@endisset
