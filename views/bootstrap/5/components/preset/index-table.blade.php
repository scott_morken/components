<?php
use Smorken\Components\Helpers\ModelToArray;
use Smorken\Components\Helpers\Value;

/**
 * @var \Illuminate\Support\Collection $models
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@props([
    'title' => null,
    'models' => new \Illuminate\Support\Collection(),
    'filter' => new \Smorken\Support\Filter(),
    'extra' => [],
    'limitAttributes' => [],
    'canCreate' => true,
    'viewColumn' => true,
    'actionsColumn' => true,
    'formFilterView' => null,
])
@if($title)
    <x-smc::title>{{ $title }}</x-smc::title>
@endif
@if($canCreate)
    <x-smc::preset.action.create></x-smc::preset.action.create>
@endif
@isset($formFilter)
    {{ $formFilter }}
@endisset
@if ($formFilterView)
    @includeIf($formFilterView)
@endif
@isset($prepend)
    {{ $prepend }}
@endisset
@if ($models && count($models))
    <x-smc::table>
        <x-slot:head>
            @isset($head)
                {{ $head }}
            @else
                @php($first = $models->first())
                @foreach ($first->getAttributes() as $attribute => $v)
                    @if (is_iterable($v) && !is_a($v, \Stringable::class))
                        @continue
                    @endif
                    @if (empty($limitAttributes) || in_array($attribute, $limitAttributes))
                        <x-smc::table.heading>
                            {{ method_exists($first, 'friendlyKey') ? $first->friendlyKey($attribute) : $attribute }}
                        </x-smc::table.heading>
                    @endif
                @endforeach
                @foreach ($extra as $attribute => $value)
                    <x-smc::table.heading>{{ $attribute }}</x-smc::table.heading>
                @endforeach
                @if ($actionsColumn)
                    <x-smc::table.heading>&nbsp;</x-smc::table.heading>
                @endif
            @endisset
        </x-slot:head>
        <x-slot:body>
            @isset($body)
                {{ $body }}
            @else
                @foreach ($models as $model)
                    @php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
                    <x-smc::table.row :model="$model">
                        @foreach (ModelToArray::from($model)->toArray() as $attribute => $value)
                            @if (is_iterable($v) && !is_a($value, \Stringable::class))
                                @continue
                            @endif
                            @if (empty($limitAttributes) || in_array($attribute, $limitAttributes))
                                @if ($attribute === $model->getKeyName() && $viewColumn)
                                    <x-smc::table.cell>
                                        <x-smc::preset.action.view
                                                title="View {{ $model->getKey() }}"
                                                :params="$params">{{ $model->getKey() }}</x-smc::preset.action.view>
                                    </x-smc::table.cell>
                                @else
                                    <x-smc::table.cell
                                            :model="\Smorken\Components\Helpers\Model::newInstance($model, $attribute)"></x-smc::table.cell>
                                @endif
                            @endif
                        @endforeach
                        @foreach ($extra as $attribute => $value)
                            <x-smc::table.cell>
                                {{ $value instanceof Closure ? $value($model) : Value::fromValue($value) }}
                            </x-smc::table.cell>
                        @endforeach
                        @if ($actionsColumn)
                            <x-smc::table.cell>
                                <x-smc::preset.action.update-delete
                                        :params="$params"></x-smc::preset.action.update-delete>
                            </x-smc::table.cell>
                        @endif
                    </x-smc::table.row>
                @endforeach
            @endisset
        </x-slot:body>
    </x-smc::table>
    <x-smc::paginate.models :models="$models" :filter="$filter"></x-smc::paginate.models>
@else
    <div class="text-muted">No records found.</div>
@endif
@isset($append)
    {{ $append }}
@endisset
