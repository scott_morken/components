@props([
    'type' => 'button',
    'href' => null,
    'title' => null,
])
<?php $href = $href ?? $attributes->get('href'); ?>
@if($href)
    @php($href = new \Illuminate\Support\HtmlString($href))
    <a href="{{ $href }}" {{ $attributes->class(['btn'])->merge(['role' => 'button']) }}
    title="{{ $title ?? e($slot) }}"
    >{{ $slot }}</a>
@else
    <button type="{{ $type }}" {{ $attributes->class(['btn']) }}
    >{{ $slot }}</button>
@endif
