<div {{ $attributes->class(['card']) }}>
    @isset($header)
        <x-smc::card.header :attributes="$header->attributes">{{ $header }}</x-smc::card.header>
    @endisset
    {{ $slot }}
    @isset($footer)
        <x-smc::card.footer :attributes="$footer->attributes">{{ $footer }}</x-smc::card.footer>
    @endisset
</div>
