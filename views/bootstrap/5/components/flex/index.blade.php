@props(['wrap' => true])
<div {{ $attributes->class(['d-flex', 'gap-3', 'flex-column', 'flex-md-row', 'flex-md-wrap' => $wrap]) }}>{{ $slot }}</div>
