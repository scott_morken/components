<?php

declare(strict_types=1);

namespace Smorken\Components\Constants;

enum ThemeType
{
    case BOOTSTRAP;

    case TAILWIND;
}
