<?php

namespace Smorken\Components\Constants;

enum Theme
{
    case BOOTSTRAP;

    case BOOTSTRAP5;

    case TAILWIND;

    case TAILWIND3;

    public function map(): string
    {
        return match ($this) {
            self::TAILWIND, self::TAILWIND3 => 'tailwind.3',
            default => 'bootstrap.5',
        };
    }
}
