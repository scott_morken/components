<?php

declare(strict_types=1);

namespace Smorken\Components\Constants;

final class CurrentTheme
{
    public static Theme $currentTheme = Theme::BOOTSTRAP;

    public static function getCurrentTheme(): Theme
    {
        return self::$currentTheme;
    }

    public static function is(ThemeType $type): bool
    {
        $data = match ($type) {
            ThemeType::BOOTSTRAP => [Theme::BOOTSTRAP, Theme::BOOTSTRAP5],
            ThemeType::TAILWIND => [Theme::TAILWIND, Theme::TAILWIND3],
        };

        return in_array(self::$currentTheme, $data, true);
    }
}
