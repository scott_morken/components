<?php

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 11:11 AM
 */

namespace Smorken\Components;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as SP;
use Smorken\Components\Constants\CurrentTheme;
use Smorken\Components\Constants\Theme;
use Smorken\Components\Constants\ThemeType;
use Smorken\Components\Helpers\FormFilter;

class ServiceProvider extends SP
{
    protected string $baseClassNamespace = 'Smorken\\Components\\Components\\';

    /**
     * Bootstrap the application events.
     */
    public function boot(): void
    {
        $this->bootViews();
        $this->bootComponentNamespace();
        $this->bootPaginator();
        $this->bootFormFilter();
    }

    protected function bootComponentNamespace(): void
    {
        $namespace = $this->baseClassNamespace.'Bootstrap';
        $prefix = 'smc';
        if (CurrentTheme::is(ThemeType::TAILWIND)) {
            $namespace = $this->baseClassNamespace.'Tailwind';
        }
        Blade::componentNamespace($namespace, $prefix);
    }

    protected function bootFormFilter(): void
    {
        if (CurrentTheme::is(ThemeType::TAILWIND)) {
            FormFilter::setForTailwind();
        }
    }

    protected function bootPaginator(): void
    {
        if (CurrentTheme::is(ThemeType::BOOTSTRAP)) {
            Paginator::useBootstrapFive();
        }
    }

    protected function bootViews(): void
    {
        $path = $this->getViewPath();
        $this->loadViewsFrom($path, 'smc');
        $this->publishes([
            $path => resource_path('/views/vendor/smorken/components'),
        ], 'views');
    }

    protected function convertThemeToPath(Theme $theme): string
    {
        return str_replace('.', DIRECTORY_SEPARATOR, $theme->map());
    }

    protected function getViewPath(): string
    {
        $path = realpath(__DIR__.'/../views/'.$this->convertThemeToPath(CurrentTheme::getCurrentTheme()));
        if ($path === false) {
            throw new \OutOfBoundsException($this->convertThemeToPath(CurrentTheme::getCurrentTheme()).' view path does not exist.');
        }

        return $path;
    }
}
