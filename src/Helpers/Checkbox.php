<?php

namespace Smorken\Components\Helpers;

class Checkbox extends Checkable implements \Smorken\Components\Contracts\Helpers\Checkbox
{
    public function __construct(
        string $value,
        string $label,
        ?bool $checked = null,
        public bool $isMultiple = false,
        public ?string $hiddenValue = null,
    ) {
        parent::__construct($value, $label, $checked);
    }

    protected static function fromValue(
        string|\Smorken\Components\Contracts\Helpers\Checkable $value,
        string|int $key
    ): \Smorken\Components\Contracts\Helpers\Checkbox {
        if ($value instanceof \Smorken\Components\Contracts\Helpers\Checkbox) {
            return $value;
        }

        return new static(
            $key,
            $value,
            null,
            true
        );
    }
}
