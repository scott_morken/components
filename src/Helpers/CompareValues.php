<?php

namespace Smorken\Components\Helpers;

class CompareValues
{
    public static function compare(string $inputValue, mixed $compareValue): bool
    {
        if ($compareValue === null) {
            return false;
        }
        if (is_array($compareValue)) {
            return in_array($inputValue, $compareValue, true);
        }
        $v = new Value($compareValue);

        return $inputValue === (string) $v;
    }
}
