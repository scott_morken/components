<?php
declare(strict_types=1);

namespace Smorken\Components\Helpers;

use Carbon\Carbon;

class Is implements \Smorken\Components\Contracts\Helpers\Is
{

    public function isArray(mixed $value): bool
    {
        return is_array($value);
    }

    public function isBackedEnum(mixed $value): bool
    {
        return is_a($value, \BackedEnum::class);
    }

    public function isBoolean(mixed $value): bool
    {
        return is_bool($value);
    }

    public function isCarbon(mixed $value): bool
    {
        return is_a($value, Carbon::class);
    }

    public function isDateTime(mixed $value): bool
    {
        return is_a($value, \DateTimeInterface::class);
    }

    public function isEmpty(mixed $value): bool
    {
        return empty($value) && ! $this->isNumeric($value) && ! $this->isBoolean($value);
    }

    public function isEnum(mixed $value): bool
    {
        return is_a($value, \UnitEnum::class);
    }

    public function isNumeric(mixed $value): bool
    {
        return is_numeric($value);
    }

    public function isObject(mixed $value): bool
    {
        return is_object($value);
    }

    public function isString(mixed $value): bool
    {
        return is_string($value);
    }
}