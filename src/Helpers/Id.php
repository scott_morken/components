<?php

namespace Smorken\Components\Helpers;

use Smorken\Support\Str;

class Id implements \Smorken\Components\Contracts\Helpers\Id
{
    public function convertToDotNotation(string $name): string
    {
        return rtrim(str_replace(['[', ']'], ['.', ''], $name), '.');
    }

    public function make(string $name): string
    {
        if ($name === '') {
            $name = 'input-'.uniqid();
        }

        return preg_replace('/[-]{2,}/', '-',
            Str::slug(str_replace(['[', ']', '_'], '-', $name)));
    }
}
