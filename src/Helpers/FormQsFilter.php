<?php

namespace Smorken\Components\Helpers;

use Illuminate\Support\ViewErrorBag;
use Smorken\QueryStringFilter\Constants\PartType;
use Smorken\QueryStringFilter\Contracts\Parts\Part;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;

class FormQsFilter implements \Smorken\Components\Contracts\Helpers\FormQsFilter
{
    protected static string $filteredClasses = 'border border-success';

    protected static string $invalidClasses = 'is-invalid';

    public function __construct(protected ViewErrorBag $errors, public QueryStringFilter $filter) {}

    public static function setForBootstrap(): void
    {
        self::$filteredClasses = 'border border-success';
        self::$invalidClasses = 'is-invalid';
    }

    public static function setForTailwind(): void {}

    public function getClasses(string $attribute, PartType $type = PartType::FILTER): string
    {
        if ($this->errors->has($this->getErrorsKey($attribute, $type))) {
            return self::$invalidClasses;
        }

        return ! ($this->getPart($attribute, $type)?->empty() ?? true) ? self::$filteredClasses : '';
    }

    public function getValue(string $attribute, PartType $type = PartType::FILTER): mixed
    {
        return $this->getPart($attribute, $type)?->get() ?? null;
    }

    protected function getErrorsKey(string $attribute, PartType $type): string
    {
        return $this->filter->qualifiedKeyName($attribute, $type);
    }

    protected function getPart(string $attribute, PartType $type): ?Part
    {
        return $this->filter->find($attribute, $type);
    }
}
