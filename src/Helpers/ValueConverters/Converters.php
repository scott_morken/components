<?php
declare(strict_types=1);

namespace Smorken\Components\Helpers\ValueConverters;

enum Converters
{
    case ARRAY;
    case BOOLEAN;
    case DATE;
    case DATE_TIME;
    case ENUM;
    case OBJECT;
}
