<?php
declare(strict_types=1);

namespace Smorken\Components\Helpers\ValueConverters;

class DateTimeConverter extends BaseConverter
{

    protected string $format = 'Y-m-d H:i:s';

    public function convert(mixed $value, bool $asFriendlyString): ?string
    {
        if ($this->is->isCarbon($value)) {
            return $asFriendlyString ? $value->toDayDateTimeString() : $value->format($this->format);
        }

        if ($this->is->isDateTime($value)) {
            return $value->format($this->format);
        }

        return (string) $value;
    }

    protected function parseConstructorArgs(...$args): void
    {
        if (isset($args[0])) {
            $this->format = $args[0];
        }
        if (isset($args['format'])) {
            $this->format = $args['format'];
        }
    }
}