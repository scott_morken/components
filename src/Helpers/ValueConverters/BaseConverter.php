<?php
declare(strict_types=1);

namespace Smorken\Components\Helpers\ValueConverters;

use Smorken\Components\Contracts\Helpers\Is;
use Smorken\Components\Contracts\Helpers\ValueConverter;

abstract class BaseConverter implements ValueConverter
{

    protected Is $is;

    public function __construct(
        ...$args
    ) {
        $this->parseConstructorArgs(...$args);
        $this->is = new \Smorken\Components\Helpers\Is();
    }

    protected function parseConstructorArgs(...$args): void
    {
        // override
    }
}