<?php
declare(strict_types=1);

namespace Smorken\Components\Helpers\ValueConverters;

class BooleanConverter extends BaseConverter
{

    public function convert(mixed $value, bool $asFriendlyString): ?string
    {
        if ($asFriendlyString) {
            return $value ? 'Yes' : 'No';
        }

        return $value ? '1' : '0';
    }
}