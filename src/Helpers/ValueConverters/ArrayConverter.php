<?php
declare(strict_types=1);

namespace Smorken\Components\Helpers\ValueConverters;

class ArrayConverter extends BaseConverter
{

    public function convert(mixed $value, bool $asFriendlyString): ?string
    {
        if ($asFriendlyString) {
            try {
                return json_encode($value, JSON_PRETTY_PRINT);
            } catch (\Throwable) {
                // continue to implode
            }
        }

        return implode('-', $value);
    }
}