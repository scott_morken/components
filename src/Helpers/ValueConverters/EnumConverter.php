<?php
declare(strict_types=1);

namespace Smorken\Components\Helpers\ValueConverters;

class EnumConverter extends BaseConverter
{

    public function convert(mixed $value, bool $asFriendlyString): ?string
    {
        if ($this->is->isBackedEnum($value)) {
            if ($asFriendlyString && method_exists($value, 'label')) {
                return $value->label();
            }

            return $value->value;
        }

        return strtolower($value->name);
    }
}
