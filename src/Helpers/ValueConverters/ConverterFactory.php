<?php
declare(strict_types=1);

namespace Smorken\Components\Helpers\ValueConverters;

use Smorken\Components\Contracts\Helpers\ValueConverter;

class ConverterFactory
{

    public static function create(Converters $converter, ...$constructorArgs): ValueConverter
    {
        return match ($converter) {
            Converters::ARRAY => new ArrayConverter(...$constructorArgs),
            Converters::BOOLEAN => new BooleanConverter(...$constructorArgs),
            Converters::DATE => new DateConverter(...$constructorArgs),
            Converters::DATE_TIME => new DateTimeConverter(...$constructorArgs),
            Converters::ENUM => new EnumConverter(...$constructorArgs),
            Converters::OBJECT => new ObjectConverter(...$constructorArgs),
        };
    }
}