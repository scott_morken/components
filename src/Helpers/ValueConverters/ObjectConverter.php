<?php
declare(strict_types=1);

namespace Smorken\Components\Helpers\ValueConverters;

use Illuminate\Contracts\Support\Jsonable;

class ObjectConverter extends BaseConverter
{

    public function convert(mixed $value, bool $asFriendlyString): ?string
    {
        if (method_exists($value, 'toString')) {
            return $value->toString();
        }
        if (is_a($value, Jsonable::class)) {
            return $value->toJson(JSON_PRETTY_PRINT);
        }

        return (string) $value;
    }
}