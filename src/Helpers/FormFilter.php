<?php

namespace Smorken\Components\Helpers;

use Illuminate\Support\ViewErrorBag;
use Smorken\Support\Contracts\Filter;

class FormFilter implements \Smorken\Components\Contracts\Helpers\FormFilter
{
    protected static string $filteredClasses = 'border border-success';

    protected static string $invalidClasses = 'is-invalid';

    public function __construct(protected ViewErrorBag $errors, public Filter $filter) {}

    public static function setForBootstrap(): void
    {
        self::$filteredClasses = 'border border-success';
        self::$invalidClasses = 'is-invalid';
    }

    public static function setForTailwind(): void {}

    public function getClasses(string $attribute): string
    {
        if ($this->errors->has($attribute)) {
            return self::$invalidClasses;
        }

        return $this->filter->isNotEmpty($attribute) ? self::$filteredClasses : '';
    }

    public function getValue(string $attribute): mixed
    {
        return $this->filter->isNotEmpty($attribute) ? $this->filter->getAttribute($attribute) : null;
    }
}
