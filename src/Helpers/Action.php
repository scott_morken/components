<?php

namespace Smorken\Components\Helpers;

class Action
{
    public static function make(string $controllerClass, ?string $actionMethod): array|string
    {
        return $actionMethod ? [$controllerClass, $actionMethod] : $controllerClass;
    }
}
