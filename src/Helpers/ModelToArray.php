<?php

declare(strict_types=1);

namespace Smorken\Components\Helpers;

class ModelToArray implements \Smorken\Components\Contracts\Helpers\ModelToArray
{
    public function __construct(protected mixed $model) {}

    public static function from(mixed $model): static
    {
        return new static($model);
    }

    public function toArray(): array
    {
        if (is_a($this->model, \Illuminate\Database\Eloquent\Model::class)) {
            return $this->fromEloquentModel();
        }
        if (is_array($this->model)) {
            return $this->model;
        }
        if (method_exists($this->model, 'toArray')) {
            return $this->model->toArray();
        }

        return (array) $this->model;
    }

    protected function fromEloquentModel(): array
    {
        $attributes = [];
        $keys = array_unique(array_keys($this->model->getAttributes()) + array_keys($this->model->getAppends()));
        foreach ($keys as $key) {
            $attributes[$key] = $this->model->getAttribute($key);
        }

        return $attributes;
    }
}
