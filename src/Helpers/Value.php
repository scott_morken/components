<?php

namespace Smorken\Components\Helpers;

use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Support\HtmlString;
use Smorken\Components\Helpers\ValueConverters\ConverterFactory;
use Smorken\Components\Helpers\ValueConverters\Converters;

class Value implements \Smorken\Components\Contracts\Helpers\Value
{
    protected array $converterConstructorArgs = [];

    protected mixed $defaultValue = '';

    protected \Smorken\Components\Contracts\Helpers\Is $is;

    protected ?Converters $useConverter = null;

    public function __construct(protected mixed $value, protected bool $friendlyString = false)
    {
        $this->is = new Is();
    }

    public static function fromValue(mixed $value, bool $friendlyString = false): static
    {
        return new static($value, $friendlyString);
    }

    public function __toString(): string
    {
        if ($this->isEmpty()) {
            return $this->handleEmptyReturn();
        }
        if ($this->useConverter) {
            return $this->convert($this->useConverter);
        }
        if ($this->isString()) {
            return (string) $this->value;
        }
        if ($this->isBoolean()) {
            return $this->convert(Converters::BOOLEAN);
        }
        if ($this->isArray()) {
            return $this->convert(Converters::ARRAY);
        }
        if ($this->isEnum()) {
            return $this->convert(Converters::ENUM);
        }
        if ($this->isDateTime()) {
            return $this->convert(Converters::DATE_TIME);
        }
        if ($this->isObject()) {
            return $this->convert(Converters::OBJECT);
        }

        return (string) $this->value;
    }

    public function htmlable(): Htmlable
    {
        $str = e($this->__toString());
        if ($this->wrapWithPre()) {
            return new HtmlString("<pre>$str</pre>");
        }

        return new HtmlString($str);
    }

    public function isArray(): bool
    {
        return $this->is->isArray($this->value);
    }

    public function isBoolean(): bool
    {
        return $this->is->isBoolean($this->value);
    }

    public function isCarbon(): bool
    {
        return $this->is->isCarbon($this->value);
    }

    public function isDateTime(): bool
    {
        return $this->is->isDateTime($this->value);
    }

    public function isEmpty(): bool
    {
        return $this->is->isEmpty($this->value);
    }

    public function isEnum(): bool
    {
        return $this->is->isEnum($this->value);
    }

    public function isNumeric(): bool
    {
        return $this->is->isNumeric($this->value);
    }

    public function isObject(): bool
    {
        return $this->is->isObject($this->value);
    }

    public function isString(): bool
    {
        return $this->is->isString($this->value);
    }

    public function value(): mixed
    {
        if ($this->isEmpty()) {
            return $this->defaultValue;
        }

        return $this->value;
    }

    public function viaCallback(\Closure $callback): static
    {
        return new static($callback($this->value));
    }

    public function withConverter(Converters $converter, ...$constructorArgs): static
    {
        $this->useConverter = $converter;

        return $this->withConverterConstructorArgs(...$constructorArgs);
    }

    public function withConverterConstructorArgs(...$constructorArgs): static
    {
        $this->converterConstructorArgs = $constructorArgs;

        return $this;
    }

    public function withDefaultValue(mixed $default): static
    {
        $this->defaultValue = $default;

        return $this;
    }

    protected function convert(Converters $converter): ?string
    {
        return ConverterFactory::create($converter, ...$this->converterConstructorArgs)
            ->convert($this->value, $this->friendlyString);
    }

    protected function handleEmptyReturn(): string
    {
        $value = new static($this->defaultValue, $this->friendlyString);
        if ($this->useConverter) {
            $value->withConverter($this->useConverter, ...$this->converterConstructorArgs);
        }
        $value->withConverterConstructorArgs(...$this->converterConstructorArgs);
        if ($value->isEmpty()) {
            return '';
        }

        return $value->__toString();
    }

    protected function wrapWithPre(): bool
    {
        if ($this->friendlyString) {
            if ($this->isDateTime() || $this->isEnum()) {
                return false;
            }

            return $this->isArray() || $this->isObject();
        }

        return false;
    }
}
