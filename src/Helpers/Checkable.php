<?php

namespace Smorken\Components\Helpers;

class Checkable implements \Smorken\Components\Contracts\Helpers\Checkable
{
    public function __construct(
        public string $value,
        public string $label,
        public ?bool $checked = null
    ) {}

    public static function fromArray(array $items): array
    {
        $from = [];
        foreach ($items as $k => $v) {
            $from[] = self::fromValue($v, $k);
        }

        return $from;
    }

    protected static function fromValue(
        string|\Smorken\Components\Contracts\Helpers\Checkable $value,
        string|int $key
    ): \Smorken\Components\Contracts\Helpers\Checkable {
        if ($value instanceof \Smorken\Components\Contracts\Helpers\Checkable) {
            return $value;
        }

        return new static(
            $key,
            $value
        );
    }

    public function isChecked(mixed $compare): bool
    {
        if ($this->checked !== null) {
            return $this->checked;
        }

        return CompareValues::compare($this->value, $compare);
    }
}
