<?php

namespace Smorken\Components\Helpers;

class Option implements \Smorken\Components\Contracts\Helpers\Option
{
    public function __construct(
        public string $value,
        public string $text,
        public ?bool $selected = null,
        public bool $disabled = false
    ) {}

    public function isSelected(mixed $compare): bool
    {
        if ($this->selected !== null) {
            return $this->selected;
        }

        return CompareValues::compare($this->value, $compare);
    }
}
