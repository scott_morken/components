<?php

namespace Smorken\Components\Helpers;

use Smorken\Components\Helpers\ValueConverters\Converters;

class Model implements \Smorken\Components\Contracts\Helpers\Model
{
    protected array $converterConstructorArgs = [];

    protected mixed $defaultValue = null;

    protected ?Converters $useConverter = null;

    public function __construct(
        public mixed $model,
        public string $attribute = '',
        protected ?\Closure $callback = null
    ) {
    }

    public static function newInstance(
        mixed $model,
        string $attribute,
        ?\Closure $callback = null
    ): \Smorken\Components\Contracts\Helpers\Model {
        return new static($model, $attribute, $callback);
    }

    public function getAttribute(string $attribute): mixed
    {
        if ($this->model === null) {
            return null;
        }
        if (is_array($this->model)) {
            return $this->model[$attribute] ?? null;
        }
        if (method_exists($this->model, 'getAttribute')) {
            return $this->model->getAttribute($attribute);
        }

        return $this->model->$attribute;
    }

    public function getValue(): \Smorken\Components\Contracts\Helpers\Value
    {
        return $this->getValueHelperObject($this->getAttribute($this->attribute));
    }

    public function getValueAsString(): string
    {
        $value = $this->getValue();

        return (string) $value;
    }

    public function guessKey(): ?string
    {
        if ($this->model === null) {
            return null;
        }
        $attribute = 'id';
        if (! is_array($this->model) && method_exists($this->model, 'getKeyName')) {
            $attribute = $this->model->getKeyName();
        }
        $value = $this->getAttribute($attribute);

        return (string) $this->getValueHelperObject($value);
    }

    public function isEmpty(): bool
    {
        return $this->model === null || ($this->model === [] && $this->attribute === '');
    }

    public function withConverter(Converters $converter, ...$constructorArgs): static
    {
        $this->useConverter = $converter;

        return $this->withConverterConstructorArgs(...$constructorArgs);
    }

    public function withConverterConstructorArgs(...$constructorArgs): static
    {
        $this->converterConstructorArgs = $constructorArgs;

        return $this;
    }

    public function withDefaultValue(mixed $default): static
    {
        $this->defaultValue = $default;

        return $this;
    }

    protected function getValueHelperObject(mixed $value): \Smorken\Components\Contracts\Helpers\Value
    {
        $v = new Value($value);
        if ($this->useConverter) {
            $v->withConverter($this->useConverter, ...$this->converterConstructorArgs);
        }
        if ($this->converterConstructorArgs) {
            $v->withConverterConstructorArgs(...$this->converterConstructorArgs);
        }
        if ($this->callback) {
            return $v->viaCallback($this->callback);
        }
        if ($this->defaultValue !== null) {
            $v = $v->withDefaultValue($this->defaultValue);
        }

        return $v;
    }
}
