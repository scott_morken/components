<?php

namespace Smorken\Components\Components\Bootstrap\Table;

use Illuminate\Contracts\View\View;
use Smorken\Components\Components\Concerns\WithValueFromRequestAndModel;
use Smorken\Components\Components\ModelBasedComponent;

class Cell extends ModelBasedComponent implements \Smorken\Components\Contracts\Components\Table\Cell
{
    use WithValueFromRequestAndModel;

    protected string $viewName = 'table.cell';

    public function render(): View
    {
        return $this->renderViewFromViewName();
    }
}
