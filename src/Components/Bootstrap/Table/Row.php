<?php

namespace Smorken\Components\Components\Bootstrap\Table;

use Illuminate\Contracts\View\View;
use Smorken\Components\Components\Concerns\WithIdUsingModel;
use Smorken\Components\Components\NamespacedComponent;

class Row extends NamespacedComponent implements \Smorken\Components\Contracts\Components\Table\Row
{
    use WithIdUsingModel;

    protected string $idPrefix = 'row-for-';

    protected string $viewName = 'table.row';

    public function __construct(mixed $model = null)
    {
        $this->model = $this->createModel($model);
    }

    public function render(): View
    {
        return $this->renderViewFromViewName();
    }
}
