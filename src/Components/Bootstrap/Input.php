<?php

namespace Smorken\Components\Components\Bootstrap;

use Illuminate\Http\Request;
use Smorken\Components\Components\InputBasedComponent;
use Smorken\Components\Contracts\Helpers\Model;

class Input extends InputBasedComponent
{
    public function __construct(
        Request $request,
        ?Model $model = null,
        public string $type = 'text',
        public bool $disabled = false,
        public bool $readonly = false,
        bool $withErrors = true
    ) {
        parent::__construct($request, $model, $withErrors);
    }
}
