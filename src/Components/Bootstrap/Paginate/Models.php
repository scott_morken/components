<?php

namespace Smorken\Components\Components\Bootstrap\Paginate;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Smorken\Components\Components\NamespacedComponent;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

class Models extends NamespacedComponent implements \Smorken\Components\Contracts\Components\Paginate\Models
{
    protected string $viewName = 'paginate.models';

    public function __construct(
        protected Paginator|Collection $models,
        protected Filter|QueryStringFilter|array|null $filter
    ) {}

    public function getAppends(array $excludes = ['page']): array
    {
        if (is_array($this->filter)) {
            return $this->filter;
        }
        if (is_a($this->filter, Filter::class)) {
            return $this->filter->except($excludes);
        }

        return $this->filter->toArray();
    }

    public function getPaginator(): Paginator
    {
        return $this->models;
    }

    public function isPaginated(): bool
    {
        return is_a($this->models, Paginator::class);
    }

    public function render(): View
    {
        return $this->renderViewFromViewName();
    }
}
