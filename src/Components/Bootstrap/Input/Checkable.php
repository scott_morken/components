<?php

namespace Smorken\Components\Components\Bootstrap\Input;

use Smorken\Components\Components\InputBasedComponent;
use Smorken\Components\Contracts\Components\Input;

abstract class Checkable extends InputBasedComponent implements Input\Checkable
{
    public function hasLabel(): bool
    {
        return $this->getCheckableHelper()?->label !== '';
    }

    public function isChecked(mixed $compare): bool
    {
        return (bool) $this->getCheckableHelper()?->isChecked($compare);
    }
}
