<?php

namespace Smorken\Components\Components\Bootstrap\Input;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Http\Request;
use Smorken\Components\Components\InputBasedComponent;
use Smorken\Components\Contracts\Helpers\Model as HelperModel;

class Checkboxes extends InputBasedComponent implements \Smorken\Components\Contracts\Components\Input\Checkboxes
{
    public array $checkboxes;

    protected bool $isMultiple = true;

    protected string $viewName = 'input.checkboxes';

    public function __construct(
        Request $request,
        iterable $checkboxes,
        EloquentModel|HelperModel|\Smorken\Model\Contracts\Model|array|null $model = null,
        bool $withErrors = true
    ) {
        parent::__construct($request, $model, $withErrors);
        $this->setCheckboxes($checkboxes);
    }

    public function render(): View
    {
        return $this->renderViewFromViewName();
    }

    protected function ensureCheckbox(
        string|\Smorken\Components\Contracts\Helpers\Checkbox $checkbox,
        string|int $key
    ): \Smorken\Components\Contracts\Helpers\Checkbox {
        if ($checkbox instanceof \Smorken\Components\Contracts\Helpers\Checkbox) {
            $checkbox->isMultiple = true;

            return $checkbox;
        }

        return new \Smorken\Components\Helpers\Checkbox($key, $checkbox, null, true);
    }

    protected function setCheckboxes(iterable $checkboxes): void
    {
        $cbs = [];
        foreach ($checkboxes as $key => $checkbox) {
            $cbs[] = $this->ensureCheckbox($checkbox, $key);
        }
        $this->checkboxes = $cbs;
    }
}
