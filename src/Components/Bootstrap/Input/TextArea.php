<?php

namespace Smorken\Components\Components\Bootstrap\Input;

use Illuminate\Http\Request;
use Smorken\Components\Components\InputBasedComponent;

class TextArea extends InputBasedComponent implements \Smorken\Components\Contracts\Components\Input\TextArea
{
    protected string $viewName = 'input.text-area';

    public function __construct(
        Request $request,
        mixed $model = null,
        public bool $disabled = false,
        public bool $readonly = false,
        bool $withErrors = true
    ) {
        parent::__construct($request, $model, $withErrors);
    }
}
