<?php

namespace Smorken\Components\Components\Bootstrap\Input;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Http\Request;
use Smorken\Components\Components\InputBasedComponent;
use Smorken\Components\Contracts\Helpers\Model as HelperModel;
use Smorken\Components\Contracts\Helpers\Option;

class SelectOptions extends InputBasedComponent implements \Smorken\Components\Contracts\Components\Input\Select
{
    /**
     * @var array<int, Option>
     */
    public array $options;

    protected string $viewName = 'input.select-options';

    public function __construct(
        Request $request,
        iterable $options,
        EloquentModel|HelperModel|\Smorken\Model\Contracts\Model|array|null $model = null,
        public bool $disabled = false,
        bool $withErrors = true
    ) {
        parent::__construct($request, $model, $withErrors);
        $this->setOptions($options);
    }

    protected function ensureOption(Option|string $option, string|int $key): Option
    {
        if ($option instanceof Option) {
            return $option;
        }

        return new \Smorken\Components\Helpers\Option($key, $option);
    }

    protected function setOptions(iterable $options): void
    {
        $opts = [];
        foreach ($options as $key => $option) {
            $opts[] = $this->ensureOption($option, $key);
        }
        $this->options = $opts;
    }
}
