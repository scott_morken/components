<?php

namespace Smorken\Components\Components\Bootstrap\Input;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Http\Request;
use Smorken\Components\Contracts\Helpers\Model as HelperModel;

class Radio extends Checkable implements \Smorken\Components\Contracts\Components\Input\Radio
{
    protected string $viewName = 'input.radio';

    public function __construct(
        Request $request,
        public \Smorken\Components\Contracts\Helpers\Checkable $checkable,
        EloquentModel|HelperModel|\Smorken\Model\Contracts\Model|array|null $model = null,
        public bool $disabled = false,
        public bool $readonly = false,
        bool $withErrors = true
    ) {
        parent::__construct($request, $model, $withErrors);
    }
}
