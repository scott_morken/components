<?php

namespace Smorken\Components\Components\Bootstrap\Input;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Http\Request;
use Smorken\Components\Components\InputBasedComponent;
use Smorken\Components\Contracts\Helpers\Model as HelperModel;

class Radios extends InputBasedComponent implements \Smorken\Components\Contracts\Components\Input\Radios
{
    public array $radios;

    protected string $viewName = 'input.radios';

    public function __construct(
        Request $request,
        iterable $radios,
        EloquentModel|HelperModel|\Smorken\Model\Contracts\Model|array|null $model = null,
        bool $withErrors = true
    ) {
        parent::__construct($request, $model, $withErrors);
        $this->setRadios($radios);
    }

    public function render(): View
    {
        return $this->renderViewFromViewName();
    }

    protected function ensureRadio(
        string|\Smorken\Components\Contracts\Helpers\Checkable $radio,
        string|int $key
    ): \Smorken\Components\Contracts\Helpers\Checkable {
        if ($radio instanceof \Smorken\Components\Contracts\Helpers\Checkable) {
            return $radio;
        }

        return new \Smorken\Components\Helpers\Checkable($key, $radio);
    }

    protected function setRadios(iterable $radios): void
    {
        $cbs = [];
        foreach ($radios as $key => $radio) {
            $cbs[] = $this->ensureRadio($radio, $key);
        }
        $this->radios = $cbs;
    }
}
