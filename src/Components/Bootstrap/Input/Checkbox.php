<?php

namespace Smorken\Components\Components\Bootstrap\Input;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Http\Request;
use Smorken\Components\Contracts\Helpers\Model as HelperModel;

class Checkbox extends Checkable implements \Smorken\Components\Contracts\Components\Input\Checkbox
{
    protected string $viewName = 'input.checkbox';

    public function __construct(
        Request $request,
        public \Smorken\Components\Contracts\Helpers\Checkbox $checkable,
        EloquentModel|HelperModel|\Smorken\Model\Contracts\Model|array|null $model = null,
        public bool $disabled = false,
        public bool $readonly = false,
        bool $withErrors = true
    ) {
        parent::__construct($request, $model, $withErrors);
        // @phpstan-ignore property.notFound
        $this->isMultiple = (bool) $this->getCheckableHelper()->isMultiple;
    }

    public function hasHiddenValue(): bool
    {
        // @phpstan-ignore property.notFound
        return $this->getCheckableHelper()->hiddenValue !== null;
    }
}
