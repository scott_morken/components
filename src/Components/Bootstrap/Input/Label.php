<?php

namespace Smorken\Components\Components\Bootstrap\Input;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Http\Request;
use Smorken\Components\Components\Concerns\WithIdUsingModel;
use Smorken\Components\Components\InputBasedComponent;
use Smorken\Components\Contracts\Helpers\Model as HelperModel;

class Label extends InputBasedComponent implements \Smorken\Components\Contracts\Components\Input\Label
{
    use WithIdUsingModel;

    protected string $viewName = 'input.label';

    public function __construct(
        Request $request,
        EloquentModel|HelperModel|\Smorken\Model\Contracts\Model|array|null $model = null,
        public string $for = '',
        public bool $visible = true
    ) {
        parent::__construct($request, $model);
    }
}
