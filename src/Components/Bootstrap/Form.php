<?php

namespace Smorken\Components\Components\Bootstrap;

use Illuminate\Contracts\View\View;
use Smorken\Components\Components\NamespacedComponent;

class Form extends NamespacedComponent implements \Smorken\Components\Contracts\Components\Form
{
    public string $method;

    protected array $noCsrfMethods = ['HEAD', 'GET', 'OPTIONS'];

    protected array $spoofMethods = ['DELETE', 'PATCH', 'PUT'];

    protected string $viewName = 'form.index';

    public function __construct(
        string $method = 'POST',
        public string $action = '',
        public bool $hasFiles = false,
        public mixed $model = null,
    ) {
        $this->method = strtoupper($method);
    }

    public function additionalAttributes(): array
    {
        $attributes = ['method' => $this->method];
        if ($this->shouldSpoof()) {
            $attributes['method'] = 'POST';
        }
        if ($this->action) {
            $attributes['action'] = $this->action;
        }
        if ($this->hasFiles) {
            $attributes['enctype'] = 'multipart/form-data';
        }

        return $attributes;
    }

    public function render(): View
    {
        return $this->renderViewFromViewName();
    }

    public function shouldSpoof(): bool
    {
        return in_array($this->method, $this->spoofMethods, true);
    }

    public function wantsCsrf(): bool
    {
        return ! in_array($this->method, $this->noCsrfMethods, true);
    }
}
