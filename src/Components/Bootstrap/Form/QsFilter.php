<?php

namespace Smorken\Components\Components\Bootstrap\Form;

use Illuminate\Contracts\View\View;
use Illuminate\Support\ViewErrorBag;
use Smorken\Components\Components\NamespacedComponent;
use Smorken\Components\Contracts\Helpers\FormQsFilter;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;

class QsFilter extends NamespacedComponent implements \Smorken\Components\Contracts\Components\Form\Filter
{
    public QueryStringFilter $filter;

    public FormQsFilter $helper;

    protected string $viewName = 'form.qs-filter';

    public function __construct(
        ?QueryStringFilter $filter = null,
        public ViewErrorBag $errors = new ViewErrorBag
    ) {
        $this->filter = $filter ?? new \Smorken\QueryStringFilter\QueryStringFilter(request());
        $this->helper = new \Smorken\Components\Helpers\FormQsFilter($this->errors, $this->filter);
    }

    public function render(): View
    {
        return $this->renderViewFromViewName();
    }
}
