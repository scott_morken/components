<?php

namespace Smorken\Components\Components\Bootstrap\Form;

use Illuminate\Contracts\View\View;
use Illuminate\Support\ViewErrorBag;
use Smorken\Components\Components\NamespacedComponent;
use Smorken\Components\Contracts\Helpers\FormFilter;

class Filter extends NamespacedComponent implements \Smorken\Components\Contracts\Components\Form\Filter
{
    public FormFilter $helper;

    protected string $viewName = 'form.filter';

    public function __construct(
        public \Smorken\Support\Contracts\Filter $filter = new \Smorken\Support\Filter,
        public ViewErrorBag $errors = new ViewErrorBag
    ) {
        $this->helper = new \Smorken\Components\Helpers\FormFilter($this->errors, $this->filter);
    }

    public function render(): View
    {
        return $this->renderViewFromViewName();
    }
}
