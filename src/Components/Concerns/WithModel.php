<?php

namespace Smorken\Components\Components\Concerns;

use Smorken\Components\Contracts\Helpers\Model as HelperModel;
use Smorken\Components\Helpers\Model;

trait WithModel
{
    public HelperModel $model;

    public function createModel(
        mixed $model,
        string $attribute = ''
    ): HelperModel {
        if ($model instanceof HelperModel) {
            return $model;
        }

        return Model::newInstance($model, $attribute);
    }

    public function getModel(): HelperModel
    {
        return $this->model;
    }

    public function hasModel(): bool
    {
        return ! $this->getModel()->isEmpty();
    }
}
