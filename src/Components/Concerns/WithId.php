<?php

namespace Smorken\Components\Components\Concerns;

use Illuminate\View\ComponentAttributeBag;
use Smorken\Components\Contracts\Helpers\Id;

trait WithId
{
    public function getId(): ?string
    {
        return $this->getIdFromAttributeBag($this->attributes);
    }

    protected function getIdFromAttributeBag(ComponentAttributeBag $bag): ?string
    {
        if (($id = $bag->get('id')) !== null) {
            return $id;
        }

        return $bag->get('name');
    }

    protected function getIdHelper(): Id
    {
        return new \Smorken\Components\Helpers\Id;
    }
}
