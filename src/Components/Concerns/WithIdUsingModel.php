<?php

namespace Smorken\Components\Components\Concerns;

trait WithIdUsingModel
{
    use WithId, WithModel;

    public function getId(): string
    {
        return $this->getIdHelper()->make($this->getIdPrefix().$this->getIdAsString());
    }

    public function hasId(): bool
    {
        return ! $this->valueIsEmpty($this->getIdAsString());
    }

    protected function getIdAsString(): string
    {
        $id = $this->attributes['id'] ?? $this->getModel()->guessKey();
        if (is_array($id)) {
            $id = implode('-', $id);
        }

        return (string) $id;
    }

    protected function getIdPrefix(): string
    {
        if (property_exists($this, 'idPrefix')) {
            return $this->idPrefix;
        }

        return '';
    }
}
