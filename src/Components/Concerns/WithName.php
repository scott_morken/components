<?php

namespace Smorken\Components\Components\Concerns;

use Illuminate\View\ComponentAttributeBag;

trait WithName
{
    public function getName(): ?string
    {
        return $this->getNameFromAttributeBag($this->attributes);
    }

    protected function getInputKey(): ?string
    {
        return $this->getInputKeyFromAttributeBag($this->attributes);
    }

    protected function getInputKeyFromAttributeBag(ComponentAttributeBag $bag): ?string
    {
        return $this->getNameFromAttributeBag($bag);
    }

    protected function getNameFromAttributeBag(ComponentAttributeBag $bag): ?string
    {
        if ($bag->has('name')) {
            return $bag->get('name');
        }

        return $bag->get('id');
    }
}
