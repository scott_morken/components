<?php

namespace Smorken\Components\Components\Concerns;

use Illuminate\Http\Request;
use Illuminate\View\ComponentAttributeBag;
use Smorken\Components\Contracts\Helpers\Model;
use Smorken\Components\Helpers\Id;

trait WithValueFromRequestAndModel
{
    use WithModel, WithName, WithRequest;

    public function getValue(bool $withOld = true, bool $asString = false): mixed
    {
        return $this->getValueFromData(
            $this->getRequest(),
            $this->attributes,
            $this->getModel(),
            $withOld,
            $asString
        );
    }

    protected function convertNameToDotNotation(string $inputKey): string
    {
        return (new Id)->convertToDotNotation($inputKey);
    }

    protected function getModelValue(?Model $model, bool $asString): mixed
    {
        if ($asString) {
            return (string) $model?->getValue();
        }

        return $model?->getValue()->value();
    }

    protected function getValueFromData(
        Request $request,
        ComponentAttributeBag $bag,
        ?Model $model,
        bool $withOld,
        bool $asString
    ): mixed {
        if ($bag->has('value')) {
            return $bag->get('value');
        }
        $inputKey = $this->getInputKeyFromAttributeBag($bag);
        $modelValue = $this->getModelValue($model, $asString);
        if ($inputKey && $withOld) {
            return $this->getValueFromRequest($request, $inputKey, $modelValue);
        }

        return $modelValue;
    }

    protected function getValueFromRequest(Request $request, string $inputKey, mixed $default): mixed
    {
        try {
            return $request->old($this->convertNameToDotNotation($inputKey), $default);
        } catch (\Throwable) {
            return $default;
        }
    }
}
