<?php

namespace Smorken\Components\Components\Concerns;

use Illuminate\Http\Request;

trait WithRequest
{
    protected Request $request;

    public function getRequest(): Request
    {
        return $this->request;
    }
}
