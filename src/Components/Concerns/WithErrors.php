<?php

namespace Smorken\Components\Components\Concerns;

use Illuminate\Support\ViewErrorBag;

trait WithErrors
{
    use WithName, WithRequest;

    protected ?ViewErrorBag $errorBag = null;

    public function getErrors(): iterable
    {
        if (! $this->wantsErrorCheck()) {
            return [];
        }
        $inputKey = $this->getInputKey();
        if ($inputKey) {
            if ($this->getErrorBag()->has($inputKey)) {
                return $this->getErrorBag()->get($inputKey);
            }
            if ($this->getErrorBag()->has($inputKey.'.*')) {
                return $this->getErrorBag()->get($inputKey.'.*');
            }
        }

        return [];
    }

    public function hasErrors(): bool
    {
        if (! $this->wantsErrorCheck()) {
            return false;
        }
        $inputKey = $this->getInputKey();
        if ($inputKey) {
            return $this->getErrorBag()->has($this->getInputKey()) ||
                $this->getErrorBag()->has($this->getInputKey().'.*');
        }

        return false;
    }

    protected function getErrorBag(): ViewErrorBag
    {
        if ($this->errorBag === null) {
            try {
                $this->errorBag = $this->getRequest()->session()->get('errors', new ViewErrorBag);
            } catch (\Throwable) {
                $this->errorBag = new ViewErrorBag;
            }
        }

        return $this->errorBag;
    }

    protected function wantsErrorCheck(): bool
    {
        if (property_exists($this, 'withErrors')) {
            return $this->withErrors;
        }

        return true;
    }
}
