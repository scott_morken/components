<?php

namespace Smorken\Components\Components;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\View\ComponentAttributeBag;
use Smorken\Components\Components\Concerns\WithErrors;
use Smorken\Components\Components\Concerns\WithId;
use Smorken\Components\Components\Concerns\WithValueFromRequestAndModel;
use Smorken\Components\Contracts\Components\Input;
use Smorken\Components\Contracts\Helpers\Checkable;
use Smorken\Components\Contracts\Helpers\Checkbox;
use Smorken\Components\Contracts\Helpers\Model;

abstract class InputBasedComponent extends ModelBasedComponent implements Input
{
    use WithErrors, WithId, WithValueFromRequestAndModel;

    protected bool $isMultiple = false;

    protected string $viewName = 'input.index';

    public function __construct(
        Request $request,
        mixed $model = null,
        public bool $withErrors = true,
    ) {
        parent::__construct($request, $model);
    }

    public function isCheckable(): bool
    {
        return $this instanceof Input\Checkable;
    }

    public function isMultiple(): bool
    {
        return $this->isMultiple;
    }

    public function render(): \Closure|View|string
    {
        return function (array $data): string {
            $this->ensureNameFormatted($data['attributes'] ?? null);
            $this->ensureIdFormatted($this->getRequest(), $data['attributes'] ?? null, $data['model'] ?? null);

            return $this->renderViaViewName();
        };
    }

    protected function ensureIdFormatted(Request $request, ?ComponentAttributeBag $bag, ?Model $model): void
    {
        if ($bag === null && (! $model || $model->isEmpty())) {
            return;
        }
        $id = $this->getIdFromAttributeBag($bag);
        if ($id === null) {
            $id = $model->attribute;
        }
        if ($id !== null && $id !== '') {
            $id = $this->ensureValueAppended($request, $bag, $model, $id);
            $bag['id'] = $this->getIdHelper()->make($id);
        }
    }

    protected function ensureNameFormatted(?ComponentAttributeBag $bag): void
    {
        if ($bag === null && ! $this->hasModel()) {
            return;
        }
        $name = $this->getNameFromAttributeBag($bag);
        if ($name === null) {
            $name = $this->getModel()->attribute;
        }
        if (! $this->valueIsEmpty($name)) {
            if ($this->isMultiple() && ! str_ends_with($name, ']')) {
                $name = $name.'[]';
            }
            $bag['name'] = $name;
        }
    }

    protected function ensureValueAppended(
        Request $request,
        ComponentAttributeBag $bag,
        ?Model $model,
        string $id
    ): string {
        if ($this->isCheckable()) {
            $value = $this->getCheckableHelper()?->value;
            if ($value) {
                $append = '-'.$value;
                if (! str_ends_with($id, $append)) {
                    $id = $id.$append;
                }
            }
        }

        return $id;
    }

    protected function getCheckableHelper(): Checkable|Checkbox|null
    {
        if (property_exists($this, 'checkable')) {
            return $this->checkable;
        }

        return null;
    }
}
