<?php

namespace Smorken\Components\Components;

use Illuminate\Http\Request;
use Smorken\Components\Components\Concerns\WithRequest;

abstract class RequestBasedComponent extends NamespacedComponent implements \Smorken\Components\Contracts\Components\Concerns\WithRequest
{
    use WithRequest;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}
