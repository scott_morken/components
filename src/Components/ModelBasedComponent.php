<?php

namespace Smorken\Components\Components;

use Illuminate\Http\Request;
use Smorken\Components\Components\Concerns\WithModel;

abstract class ModelBasedComponent extends RequestBasedComponent implements \Smorken\Components\Contracts\Components\Concerns\WithModel
{
    use WithModel;

    public function __construct(Request $request, mixed $model = null)
    {
        parent::__construct($request);
        $this->model = $this->createModel($model);
    }
}
