<?php

namespace Smorken\Components\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

abstract class NamespacedComponent extends Component
{
    protected string $viewName;

    public function getViewName(string $component): string
    {
        return 'smc::components.'.$component;
    }

    protected function renderViaViewName(): string
    {
        return $this->getViewName($this->viewName);
    }

    protected function renderViewFromViewName(): View
    {
        return $this->factory()->make($this->renderViaViewName());
    }

    protected function valueIsEmpty(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value) && ! is_bool($value);
    }
}
