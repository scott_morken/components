<?php

namespace Smorken\Components\Contracts\Helpers;

/**
 * @property string $value
 * @property string $label
 * @property ?bool $checked
 *
 * @phpstan-require-extends \Smorken\Components\Helpers\Checkable
 */
interface Checkable
{
    public function __construct(
        string $value,
        string $label,
        ?bool $checked = null
    );

    public function isChecked(mixed $compare): bool;

    public static function fromArray(array $items): array;
}
