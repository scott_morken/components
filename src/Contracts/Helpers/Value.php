<?php

namespace Smorken\Components\Contracts\Helpers;

use Illuminate\Contracts\Support\Htmlable;
use Smorken\Components\Helpers\ValueConverters\Converters;

/**
 * @property mixed $value
 *
 * @phpstan-require-extends \Smorken\Components\Helpers\Value
 */
interface Value extends \Stringable
{
    public function __construct(mixed $value);

    public function htmlable(): Htmlable;

    public function isArray(): bool;

    public function isBoolean(): bool;

    public function isCarbon(): bool;

    public function isDateTime(): bool;

    public function isEmpty(): bool;

    public function isEnum(): bool;

    public function isNumeric(): bool;

    public function isObject(): bool;

    public function isString(): bool;

    public function value(): mixed;

    public function viaCallback(\Closure $callback): static;

    public function withConverter(Converters $converter, ...$constructorArgs): static;

    public function withConverterConstructorArgs(...$constructorArgs): static;

    public function withDefaultValue(mixed $default): static;

    public static function fromValue(mixed $value, bool $friendlyString = false): static;
}
