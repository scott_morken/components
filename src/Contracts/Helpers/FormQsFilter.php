<?php

namespace Smorken\Components\Contracts\Helpers;

use Smorken\QueryStringFilter\Constants\PartType;

/**
 * @property ?\Smorken\QueryStringFilter\Contracts\QueryStringFilter $filter
 *
 * @phpstan-require-extends \Smorken\Components\Helpers\FormQsFilter
 */
interface FormQsFilter
{
    public function getClasses(string $attribute, PartType $type = PartType::FILTER): string;

    public function getValue(string $attribute, PartType $type = PartType::FILTER): mixed;

    public static function setForBootstrap(): void;

    public static function setForTailwind(): void;
}
