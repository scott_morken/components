<?php

namespace Smorken\Components\Contracts\Helpers;

/**
 * @property ?\Smorken\Support\Contracts\Filter $filter
 *
 * @phpstan-require-extends \Smorken\Components\Helpers\FormFilter
 */
interface FormFilter
{
    public function getClasses(string $attribute): string;

    public function getValue(string $attribute): mixed;

    public static function setForBootstrap(): void;

    public static function setForTailwind(): void;
}
