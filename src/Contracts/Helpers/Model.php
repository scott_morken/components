<?php

namespace Smorken\Components\Contracts\Helpers;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Components\Helpers\ValueConverters\Converters;
use Smorken\Data\Contracts\Data;

/**
 * @property EloquentModel|\Smorken\Model\Contracts\Model|Data|array|null $model
 * @property string $attribute
 *
 * @phpstan-require-extends \Smorken\Components\Helpers\Model
 */
interface Model
{
    public function __construct(
        mixed $model,
        string $attribute = '',
        ?\Closure $callback = null
    );

    public function getAttribute(string $attribute): mixed;

    public function getValue(): Value;

    public function getValueAsString(): string;

    public function guessKey(): ?string;

    public function isEmpty(): bool;

    public function withConverter(Converters $converter, ...$constructorArgs): static;

    public function withConverterConstructorArgs(...$constructorArgs): static;

    public function withDefaultValue(mixed $default): static;

    public static function newInstance(
        EloquentModel|\Smorken\Model\Contracts\Model|Data|array|null $model,
        string $attribute,
        ?\Closure $callback = null
    ): self;
}
