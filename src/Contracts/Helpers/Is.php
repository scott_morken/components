<?php
declare(strict_types=1);

namespace Smorken\Components\Contracts\Helpers;

interface Is
{

    public function isArray(mixed $value): bool;

    public function isBackedEnum(mixed $value): bool;

    public function isBoolean(mixed $value): bool;

    public function isCarbon(mixed $value): bool;

    public function isDateTime(mixed $value): bool;

    public function isEmpty(mixed $value): bool;

    public function isEnum(mixed $value): bool;

    public function isNumeric(mixed $value): bool;

    public function isObject(mixed $value): bool;

    public function isString(mixed $value): bool;
}