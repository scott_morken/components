<?php

namespace Smorken\Components\Contracts\Helpers;

/**
 * @property string $value
 * @property string $text
 * @property ?bool $selected
 * @property bool $disabled
 *
 * @phpstan-require-extends \Smorken\Components\Helpers\Option
 */
interface Option
{
    public function isSelected(mixed $compare): bool;
}
