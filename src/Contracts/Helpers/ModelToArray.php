<?php

declare(strict_types=1);

namespace Smorken\Components\Contracts\Helpers;

use Illuminate\Contracts\Support\Arrayable;

interface ModelToArray extends Arrayable
{
    public function __construct(mixed $model);

    public function toArray(): array;

    public static function from(mixed $model): static;
}
