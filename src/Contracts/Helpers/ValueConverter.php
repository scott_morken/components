<?php
declare(strict_types=1);

namespace Smorken\Components\Contracts\Helpers;

interface ValueConverter
{

    public function convert(mixed $value, bool $asFriendlyString): ?string;
}