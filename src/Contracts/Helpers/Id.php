<?php

namespace Smorken\Components\Contracts\Helpers;

interface Id
{
    public function convertToDotNotation(string $name): string;

    public function make(string $name): string;
}
