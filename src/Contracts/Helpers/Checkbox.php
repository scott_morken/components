<?php

namespace Smorken\Components\Contracts\Helpers;

/**
 * @property string $value
 * @property string $label
 * @property bool $isMultiple
 * @property ?bool $checked
 * @property ?string $hiddenValue
 *
 * @phpstan-require-extends \Smorken\Components\Helpers\Checkbox
 */
interface Checkbox extends Checkable
{
    public function __construct(
        string $value,
        string $label,
        ?bool $checked = null,
        bool $isMultiple = false,
        ?string $hiddenValue = null,
    );
}
