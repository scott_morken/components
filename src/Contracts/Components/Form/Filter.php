<?php

namespace Smorken\Components\Contracts\Components\Form;

/**
 * @property \Smorken\Support\Contracts\Filter $filter
 * @property \Illuminate\Support\ViewErrorBag $errors
 * @property string $internalComponent
 * @property \Smorken\Components\Contracts\Helpers\FormFilter $helper
 */
interface Filter {}
