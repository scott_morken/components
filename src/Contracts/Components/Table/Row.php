<?php

namespace Smorken\Components\Contracts\Components\Table;

use Smorken\Components\Contracts\Components\Concerns\WithId;
use Smorken\Components\Contracts\Components\Concerns\WithModel;

interface Row extends WithId, WithModel
{
    public function hasId(): bool;
}
