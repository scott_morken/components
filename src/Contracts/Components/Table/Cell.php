<?php

namespace Smorken\Components\Contracts\Components\Table;

use Smorken\Components\Contracts\Components\Concerns\WithValueFromRequestAndModel;

interface Cell extends WithValueFromRequestAndModel {}
