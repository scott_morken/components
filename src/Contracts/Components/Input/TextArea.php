<?php

namespace Smorken\Components\Contracts\Components\Input;

use Smorken\Components\Contracts\Components\Concerns\WithErrors;
use Smorken\Components\Contracts\Components\Concerns\WithId;
use Smorken\Components\Contracts\Components\Concerns\WithValueFromRequestAndModel;

interface TextArea extends WithErrors, WithId, WithValueFromRequestAndModel {}
