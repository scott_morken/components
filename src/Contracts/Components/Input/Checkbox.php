<?php

namespace Smorken\Components\Contracts\Components\Input;

interface Checkbox extends Checkable
{
    public function hasHiddenValue(): bool;
}
