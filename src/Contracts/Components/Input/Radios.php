<?php

namespace Smorken\Components\Contracts\Components\Input;

use Smorken\Components\Contracts\Components\Concerns\WithErrors;
use Smorken\Components\Contracts\Components\Concerns\WithValueFromRequestAndModel;

interface Radios extends WithErrors, WithValueFromRequestAndModel {}
