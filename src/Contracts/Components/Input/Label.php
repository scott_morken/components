<?php

namespace Smorken\Components\Contracts\Components\Input;

use Smorken\Components\Contracts\Components\Concerns\WithId;
use Smorken\Components\Contracts\Components\Concerns\WithModel;

interface Label extends WithId, WithModel {}
