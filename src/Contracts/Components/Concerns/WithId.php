<?php

namespace Smorken\Components\Contracts\Components\Concerns;

interface WithId
{
    public function getId(): ?string;
}
