<?php

namespace Smorken\Components\Contracts\Components\Concerns;

interface WithErrors extends WithRequest
{
    public function getErrors(): iterable;

    public function hasErrors(): bool;
}
