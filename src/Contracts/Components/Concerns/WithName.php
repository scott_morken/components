<?php

namespace Smorken\Components\Contracts\Components\Concerns;

interface WithName
{
    public function getName(): ?string;
}
