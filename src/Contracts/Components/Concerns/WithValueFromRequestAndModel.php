<?php

namespace Smorken\Components\Contracts\Components\Concerns;

interface WithValueFromRequestAndModel extends WithModel, WithName, WithRequest
{
    public function getValue(): mixed;
}
