<?php

namespace Smorken\Components\Contracts\Components\Concerns;

use Smorken\Components\Contracts\Helpers\Model as HelperModel;

interface WithModel
{
    public function createModel(
        mixed $model,
        string $attribute = ''
    ): HelperModel;

    public function getModel(): HelperModel;

    public function hasModel(): bool;
}
