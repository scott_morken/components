<?php

namespace Smorken\Components\Contracts\Components\Concerns;

use Illuminate\Http\Request;

interface WithRequest
{
    public function getRequest(): Request;
}
