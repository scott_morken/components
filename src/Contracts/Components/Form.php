<?php

namespace Smorken\Components\Contracts\Components;

interface Form
{
    public function additionalAttributes(): array;

    public function shouldSpoof(): bool;

    public function wantsCsrf(): bool;
}
