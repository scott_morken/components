<?php

namespace Smorken\Components\Contracts\Components;

use Smorken\Components\Contracts\Components\Concerns\WithErrors;
use Smorken\Components\Contracts\Components\Concerns\WithId;
use Smorken\Components\Contracts\Components\Concerns\WithValueFromRequestAndModel;

interface Input extends WithErrors, WithId, WithValueFromRequestAndModel
{
    public function isCheckable(): bool;

    public function isMultiple(): bool;
}
