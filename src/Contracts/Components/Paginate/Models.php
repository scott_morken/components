<?php

namespace Smorken\Components\Contracts\Components\Paginate;

use Illuminate\Contracts\Pagination\Paginator;

interface Models
{
    public function getAppends(array $excludes = ['page']): array;

    public function getPaginator(): Paginator;

    public function isPaginated(): bool;
}
