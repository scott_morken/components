<?php

namespace Tests\Smorken\Components\Concerns;

trait NormalizeString
{
    protected function normalize(string $string): string
    {
        $parts = array_filter(array_map('trim', explode(PHP_EOL, $string)));

        return str_replace(['> <', ' >', ' <', ' />'], ['><', '>', '<', '/>'], preg_replace('/\s{2,}/', ' ', implode(' ', $parts)));
    }
}
