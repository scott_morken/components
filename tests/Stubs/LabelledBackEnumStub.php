<?php

declare(strict_types=1);

namespace Tests\Smorken\Components\Stubs;

use Smorken\Support\Constants\Attributes\Concerns\EnumHasLabel;
use Smorken\Support\Constants\Attributes\EnumLabel;

enum LabelledBackEnumStub: string
{
    use EnumHasLabel;

    #[EnumLabel('Is Fizzy')]
    case FIZ = 'fizzy';
    #[EnumLabel('Is Buzzy')]
    case BUZ = 'buzzy';
}
