<?php

namespace Tests\Smorken\Components\Stubs;

use Smorken\Model\Eloquent;

class ModelStub extends Eloquent {}
