<?php

namespace Tests\Smorken\Components\Stubs;

use Smorken\Data\Data;

class DataObjectStub extends Data
{
    public function __construct(public string $foo) {}
}
