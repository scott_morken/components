<?php

namespace Tests\Smorken\Components\Stubs;

use Illuminate\Routing\Controller;

class InvokableController extends Controller
{
    public function __invoke() {}
}
