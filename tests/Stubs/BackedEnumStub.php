<?php

namespace Tests\Smorken\Components\Stubs;

enum BackedEnumStub: string
{
    case FIZ = 'fizzy';
    case BUZ = 'buzzy';
}
