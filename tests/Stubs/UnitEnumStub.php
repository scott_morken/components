<?php

namespace Tests\Smorken\Components\Stubs;

enum UnitEnumStub
{
    case FOO;

    case BAR;
}
