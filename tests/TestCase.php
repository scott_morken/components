<?php

namespace Tests\Smorken\Components;

use Illuminate\Foundation\Testing\Concerns\InteractsWithViews;
use Laravel\Socialite\SocialiteServiceProvider;
use Smorken\Components\ServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    use InteractsWithViews;

    protected function getPackageProviders($app): array
    {
        return [
            ServiceProvider::class,
            \Smorken\Menu\ServiceProvider::class,
            SocialiteServiceProvider::class,
            \Smorken\SocialAuth\ServiceProvider::class,
        ];
    }
}
