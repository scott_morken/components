<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Form;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\Stubs\InvokableController;
use Tests\Smorken\Components\TestCase;

class FilterButtonsTest extends TestCase
{
    use NormalizeString;

    public function test_with_invokable_reset(): void
    {
        \Illuminate\Support\Facades\Route::get('/test', InvokableController::class);
        $view = $this->blade('<x-smc::form.filter-buttons :controller="$controller" resetMethod=""></x-smc::preset.title-back>',
            [
                'controller' => InvokableController::class,
            ]
        );
        $html = $this->normalize($view);
        $expected = '<div><button type="submit" class="btn btn-primary me-2">Filter</button><a href="http://localhost/test" title="Reset filters" class="btn btn-outline-danger">Reset</a></div>';
        $this->assertEquals($expected, $html);
    }
}
