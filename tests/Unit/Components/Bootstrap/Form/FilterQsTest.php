<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Form;

use Illuminate\Http\Request;
use Smorken\QueryStringFilter\Parts\Filter;
use Smorken\QueryStringFilter\QueryStringFilter;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class FilterQsTest extends TestCase
{
    use NormalizeString;

    public function test_default(): void
    {
        $view = $this->blade('<x-smc::form.qs-filter></x-smc::form.qs-filter>');
        $html = $this->normalize($view);
        $expected = '<div class="card my-2"><div class="card-body"><form method="GET"></form></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_helper_passed_to_slot(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $filter = QueryStringFilter::from($request)
            ->setFilters([new Filter('foo')]);
        $view = $this->blade('<x-smc::form.qs-filter :filter="$filter">{{ $component->helper->getValue(\'foo\') }}</x-smc::form.qs-filter>', ['filter' => $filter]);
        $html = $this->normalize($view);
        $expected = '<div class="card my-2"><div class="card-body"><form method="GET"> bar</form></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_sample(): void
    {
        $request = (new Request)->merge(['filter' => ['name' => 'bar']]);
        $filter = QueryStringFilter::from($request)
            ->setFilters([new Filter('name'), new Filter('active')]);
        $view = $this->blade('<x-smc::_sample.qs-filter :filter="$filter"></x-smc::_sample.qs-filter>', ['filter' => $filter]);
        $html = $this->normalize($view);
        $expected = '<div class="card my-2"><div class="card-body"><form method="GET"><div class="d-flex gap-3 flex-column flex-md-row flex-md-wrap justify-content-start"><div class="mb-2"><label for="filter-name" class="form-label visually-hidden">Name</label><input type="text" maxlength="255" class="form-control border border-success" name="filter[name]" placeholder="Name..." id="filter-name" value="bar"/></div><div class="mb-2"><label for="filter-active" class="form-label visually-hidden">Status</label><select class="form-select" name="filter[active]" id="filter-active"><option value="">-- Any Status --</option><option value="1">Active</option><option value="0">Inactive</option></select></div><div><button type="submit" class="btn btn-primary me-2">Filter</button></div></div></form></div></div>';
        $this->assertEquals($expected, $html);
    }
}
