<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Form;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class FilterTest extends TestCase
{
    use NormalizeString;

    public function test_default(): void
    {
        $view = $this->blade('<x-smc::form.filter></x-smc::form.filter>');
        $html = $this->normalize($view);
        $expected = '<div class="card my-2"><div class="card-body"><form method="GET"></form></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_helper_passed_to_slot(): void
    {
        $filter = new \Smorken\Support\Filter(['foo' => 'bar']);
        $view = $this->blade('<x-smc::form.filter :filter="$filter">{{ $component->helper->getValue(\'foo\') }}</x-smc::form.filter>', ['filter' => $filter]);
        $html = $this->normalize($view);
        $expected = '<div class="card my-2"><div class="card-body"><form method="GET"> bar</form></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_sample(): void
    {
        $filter = new \Smorken\Support\Filter(['f_foo' => 'bar']);
        $view = $this->blade('<x-smc::_sample.filter :filter="$filter"></x-smc::_sample.filter>', ['filter' => $filter]);
        $html = $this->normalize($view);
        $expected = '<div class="card my-2"><div class="card-body"><form method="GET"><div class="d-flex gap-3 flex-column flex-md-row flex-md-wrap justify-content-start"><div class="mb-2"><label for="f_name" class="form-label visually-hidden">Name</label><input type="text" maxlength="255" class="form-control" name="f_name" placeholder="Name..." id="f-name" value=""/></div><div class="mb-2"><label for="f_active" class="form-label visually-hidden">Status</label><select class="form-select" name="f_active" id="f-active"><option value="">-- Any Status --</option><option value="1">Active</option><option value="0">Inactive</option></select></div><div><button type="submit" class="btn btn-primary me-2">Filter</button></div></div></form></div></div>';
        $this->assertEquals($expected, $html);
    }
}
