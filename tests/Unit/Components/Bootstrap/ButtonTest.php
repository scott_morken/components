<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class ButtonTest extends TestCase
{
    use NormalizeString;

    public function test_button(): void
    {
        $view = $this->blade('<x-smc::button>Test</x-smc::button>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn">Test</button>', $html);
    }

    public function test_button_with_classes(): void
    {
        $view = $this->blade('<x-smc::button class="btn-other">Test</x-smc::button>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-other">Test</button>', $html);
    }

    public function test_button_with_other_attributes(): void
    {
        $view = $this->blade('<x-smc::button role="link">Test</x-smc::button>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn" role="link">Test</button>', $html);
    }

    public function test_button_as_link(): void
    {
        $view = $this->blade('<x-smc::button href="foo">Test</x-smc::button>');
        $html = $this->normalize($view);
        $this->assertEquals('<a href="foo" role="button" class="btn" title="Test">Test</a>', $html);
    }

    public function test_button_as_link_with_classes(): void
    {
        $view = $this->blade('<x-smc::button class="btn-other" href="foo">Test</x-smc::button>');
        $html = $this->normalize($view);
        $this->assertEquals('<a href="foo" role="button" class="btn btn-other" title="Test">Test</a>', $html);
    }

    public function test_button_as_link_with_other_attributes(): void
    {
        $view = $this->blade('<x-smc::button disabled href="foo">Test</x-smc::button>');
        $html = $this->normalize($view);
        $this->assertEquals('<a href="foo" role="button" class="btn" disabled="disabled" title="Test">Test</a>', $html);
    }
}
