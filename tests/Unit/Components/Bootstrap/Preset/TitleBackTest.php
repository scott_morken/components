<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Preset;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\Stubs\InvokableController;
use Tests\Smorken\Components\TestCase;

class TitleBackTest extends TestCase
{
    use NormalizeString;

    public function test_with_different_back_title(): void
    {
        \Illuminate\Support\Facades\Route::get('/test', InvokableController::class);
        $view = $this->blade('<x-smc::preset.title-back :controller="$controller" actionMethod="" backTitle="Over there"></x-smc::preset.title-back>',
            [
                'controller' => InvokableController::class,
            ]
        );
        $html = $this->normalize($view);
        $expected = '<div class="d-flex gap-3 flex-column flex-md-row flex-md-wrap justify-content-between"><div><h1></h1></div><div><a href="http://localhost/test" title="Over there">Over there</a></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_invokable_controller(): void
    {
        \Illuminate\Support\Facades\Route::get('/test', InvokableController::class);
        $view = $this->blade('<x-smc::preset.title-back :controller="$controller" actionMethod=""></x-smc::preset.title-back>',
            [
                'controller' => InvokableController::class,
            ]
        );
        $html = $this->normalize($view);
        $expected = '<div class="d-flex gap-3 flex-column flex-md-row flex-md-wrap justify-content-between"><div><h1></h1></div><div><a href="http://localhost/test" title="Back">Back</a></div></div>';
        $this->assertEquals($expected, $html);
    }
}
