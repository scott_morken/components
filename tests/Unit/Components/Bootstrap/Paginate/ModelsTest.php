<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Paginate;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\QueryStringFilter\Parts\Filter;
use Smorken\QueryStringFilter\QueryStringFilter;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\Stubs\ModelStub;
use Tests\Smorken\Components\TestCase;

class ModelsTest extends TestCase
{
    use NormalizeString;

    public function test_not_paginated(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $filter = QueryStringFilter::from($request)
            ->setFilters([new Filter('foo')]);
        $view = $this->blade('<x-smc::paginate.models :filter="$filter" :models="$models"></x-smc::paginate.models>', ['filter' => $filter, 'models' => new Collection]);
        $html = $this->normalize($view);
        $expected = '';
        $this->assertEquals($expected, $html);
    }

    public function test_paginated(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar'], 'page' => 2]);
        $filter = QueryStringFilter::from($request)
            ->setFilters([new Filter('foo')]);
        $m = array_fill(0, 60, new ModelStub);
        $models = new Paginator(new Collection($m), 20, 2);
        $view = $this->blade('<x-smc::paginate.models :filter="$filter" :models="$models"></x-smc::paginate.models>', ['filter' => $filter, 'models' => $models]);
        $html = $this->normalize($view);
        $expected = '<div><nav role="navigation" aria-label="Pagination Navigation"><ul class="pagination"><li class="page-item"><a class="page-link" href="/?filter%5Bfoo%5D=bar&amp;page=1" rel="prev"> &laquo; Previous</a></li><li class="page-item"><a class="page-link" href="/?filter%5Bfoo%5D=bar&amp;page=3" rel="next">Next &raquo;</a></li></ul></nav></div>';
        $this->assertEquals($expected, $html);
    }
}
