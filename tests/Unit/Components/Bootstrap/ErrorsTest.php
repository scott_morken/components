<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap;

use Tests\Smorken\Components\TestCase;

class ErrorsTest extends TestCase
{
    public function test_errors_with_errors(): void
    {
        $view = $this->withViewErrors([
            'test' => ['Error 1', 'Error 2'],
        ])
            ->blade('<x-smc::errors></x-smc::errors>');
        $view->assertSeeTextInOrder(['Errors were found.', 'Error 1', 'Error 2'], $view);
    }

    public function test_errors_without_errors(): void
    {
        $view = $this->blade('<x-smc::errors></x-smc::errors>');
        $this->assertEquals('', $view);
    }
}
