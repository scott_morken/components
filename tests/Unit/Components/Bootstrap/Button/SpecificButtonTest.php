<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Button;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class SpecificButtonTest extends TestCase
{
    use NormalizeString;

    public function test_danger_button(): void
    {
        $view = $this->blade('<x-smc::button.danger>Test</x-smc::button.danger>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-danger">Test</button>', $html);
    }

    public function test_dark_button(): void
    {
        $view = $this->blade('<x-smc::button.dark>Test</x-smc::button.dark>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-dark">Test</button>', $html);
    }

    public function test_info_button(): void
    {
        $view = $this->blade('<x-smc::button.info>Test</x-smc::button.info>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-info">Test</button>', $html);
    }

    public function test_light_button(): void
    {
        $view = $this->blade('<x-smc::button.light>Test</x-smc::button.light>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-light">Test</button>', $html);
    }

    public function test_link_button(): void
    {
        $view = $this->blade('<x-smc::button.link>Test</x-smc::button.link>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-link">Test</button>', $html);
    }

    public function test_outline_danger_button(): void
    {
        $view = $this->blade('<x-smc::button.outline-danger>Test</x-smc::button.outline-danger>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-outline-danger">Test</button>', $html);
    }

    public function test_outline_dark_button(): void
    {
        $view = $this->blade('<x-smc::button.outline-dark>Test</x-smc::button.outline-dark>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-outline-dark">Test</button>', $html);
    }

    public function test_outline_info_button(): void
    {
        $view = $this->blade('<x-smc::button.outline-info>Test</x-smc::button.outline-info>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-outline-info">Test</button>', $html);
    }

    public function test_outline_light_button(): void
    {
        $view = $this->blade('<x-smc::button.outline-light>Test</x-smc::button.outline-light>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-outline-light">Test</button>', $html);
    }

    public function test_outline_primary_button(): void
    {
        $view = $this->blade('<x-smc::button.outline-primary>Test</x-smc::button.outline-primary>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-outline-primary">Test</button>', $html);
    }

    public function test_outline_secondary_button(): void
    {
        $view = $this->blade('<x-smc::button.outline-secondary>Test</x-smc::button.outline-secondary>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-outline-secondary">Test</button>', $html);
    }

    public function test_outline_success_button(): void
    {
        $view = $this->blade('<x-smc::button.outline-success>Test</x-smc::button.outline-success>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-outline-success">Test</button>', $html);
    }

    public function test_outline_warning_button(): void
    {
        $view = $this->blade('<x-smc::button.outline-warning>Test</x-smc::button.outline-warning>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-outline-warning">Test</button>', $html);
    }

    public function test_primary_button(): void
    {
        $view = $this->blade('<x-smc::button.primary>Test</x-smc::button.primary>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-primary">Test</button>', $html);
    }

    public function test_secondary_button(): void
    {
        $view = $this->blade('<x-smc::button.secondary>Test</x-smc::button.secondary>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-secondary">Test</button>', $html);
    }

    public function test_success_button(): void
    {
        $view = $this->blade('<x-smc::button.success>Test</x-smc::button.success>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-success">Test</button>', $html);
    }

    public function test_warning_button(): void
    {
        $view = $this->blade('<x-smc::button.warning>Test</x-smc::button.warning>');
        $html = $this->normalize($view);
        $this->assertEquals('<button type="button" class="btn btn-warning">Test</button>', $html);
    }
}
