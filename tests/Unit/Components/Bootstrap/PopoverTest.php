<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap;

use Illuminate\Support\HtmlString;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class PopoverTest extends TestCase
{
    use NormalizeString;

    public function test_popover(): void
    {
        $view = $this->blade('<x-smc::popover content="Something something"/>');
        $html = $this->normalize($view);
        $expected = '<a tabindex="0" role="button" class="popover-dismiss" data-bs-toggle="popover" data-bs-trigger="focus" data-bs-title="Info" data-bs-content="Something something">Info</a>';
        $this->assertEquals($expected, $html);
    }

    public function test_popover_with_title(): void
    {
        $view = $this->blade('<x-smc::popover content="Something something" title="foo"/>');
        $html = $this->normalize($view);
        $expected = '<a tabindex="0" role="button" class="popover-dismiss" data-bs-toggle="popover" data-bs-trigger="focus" data-bs-title="foo" data-bs-content="Something something">foo</a>';
        $this->assertEquals($expected, $html);
    }

    public function test_popover_with_slot(): void
    {
        $view = $this->blade('<x-smc::popover content="Something something">Test</x-smc::popover>');
        $html = $this->normalize($view);
        $expected = '<a tabindex="0" role="button" class="popover-dismiss" data-bs-toggle="popover" data-bs-trigger="focus" data-bs-title="Info" data-bs-content="Something something">Test</a>';
        $this->assertEquals($expected, $html);
    }

    public function test_popover_with_html_content(): void
    {
        $view = $this->blade('<x-smc::popover :content="$content">Test</x-smc::popover>', ['content' => new HtmlString('<div>Something</div>')]);
        $html = $this->normalize($view);
        $expected = '<a tabindex="0" role="button" class="popover-dismiss" data-bs-toggle="popover" data-bs-trigger="focus" data-bs-title="Info" data-bs-content="<div>Something</div>">Test</a>';
        $this->assertEquals($expected, $html);
    }
}
