<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class CardTest extends TestCase
{
    use NormalizeString;

    public function test_default(): void
    {
        $view = $this->blade('<x-smc::card></x-smc::card>');
        $html = $this->normalize($view);
        $expected = '<div class="card"></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_header_body_footer(): void
    {
        $view = $this->blade('<x-smc::card><x-slot:header>Foo</x-slot:header><x-smc::card.body>Bar</x-smc::card.body><x-slot:footer>Biz</x-slot:footer></x-smc::card>');
        $html = $this->normalize($view);
        $expected = '<div class="card"><div class="card-header">Foo</div><div class="card-body">Bar</div><div class="card-footer">Biz</div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_attributes_on_header(): void
    {
        $view = $this->blade('<x-smc::card><x-slot:header class="foo">Foo</x-slot:header></x-smc::card>');
        $html = $this->normalize($view);
        $expected = '<div class="card"><div class="card-header foo">Foo</div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_attributes_on_footer(): void
    {
        $view = $this->blade('<x-smc::card><x-slot:footer class="foo">Foo</x-slot:footer></x-smc::card>');
        $html = $this->normalize($view);
        $expected = '<div class="card"><div class="card-footer foo">Foo</div></div>';
        $this->assertEquals($expected, $html);
    }
}
