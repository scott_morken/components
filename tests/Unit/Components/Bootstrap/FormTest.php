<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class FormTest extends TestCase
{
    use NormalizeString;

    public function test_form_defaults(): void
    {
        $view = $this->blade('<x-smc::form></x-smc::form>');
        $html = $this->normalize($view);
        $this->assertEquals('<form method="POST"><input type="hidden" name="_token" value="" autocomplete="off"></form>', $html);
    }

    public function test_form_with_action(): void
    {
        $view = $this->blade('<x-smc::form action="/foo"></x-smc::form>');
        $html = $this->normalize($view);
        $expected = '<form method="POST" action="/foo"><input type="hidden" name="_token" value="" autocomplete="off"></form>';
        $this->assertEquals($expected, $html);
    }

    public function test_form_without_csrf(): void
    {
        $view = $this->blade('<x-smc::form method="get"></x-smc::form>');
        $html = $this->normalize($view);
        $expected = '<form method="GET"></form>';
        $this->assertEquals($expected, $html);
    }

    public function test_form_with_spoof(): void
    {
        $view = $this->blade('<x-smc::form method="delete"></x-smc::form>');
        $html = $this->normalize($view);
        $expected = '<form method="POST"><input type="hidden" name="_token" value="" autocomplete="off"><input type="hidden" name="_method" value="DELETE"></form>';
        $this->assertEquals($expected, $html);
    }
}
