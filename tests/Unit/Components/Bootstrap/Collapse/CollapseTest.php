<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Collapse;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class CollapseTest extends TestCase
{
    use NormalizeString;

    public function test_default(): void
    {
        $view = $this->blade('<x-smc::collapse containerId="foo"><x-slot:action>Click me</x-slot:action></x-smc::collapse>');
        $html = $this->normalize($view);
        $expected = '<div><a href="#foo" title="Click me" data-bs-toggle="collapse" aria-expanded="false" aria-controls="foo" data-bs-target="#foo" role="button">Click me</a><div class="collapse" id="foo"></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_default_show_true(): void
    {
        $view = $this->blade('<x-smc::collapse containerId="foo" :show="true"><x-slot:action>Click me</x-slot:action></x-smc::collapse>');
        $html = $this->normalize($view);
        $expected = '<div><a href="#foo" title="Click me" data-bs-toggle="collapse" aria-expanded="true" aria-controls="foo" data-bs-target="#foo" role="button">Click me</a><div class="collapse show" id="foo"></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_action_attributes(): void
    {
        $view = $this->blade('<x-smc::collapse containerId="foo"><x-slot:action class="btn btn-success">Click me</x-slot:action></x-smc::collapse>');
        $html = $this->normalize($view);
        $expected = '<div><a href="#foo" title="Click me" class="btn btn-success" data-bs-toggle="collapse" aria-expanded="false" aria-controls="foo" data-bs-target="#foo" role="button">Click me</a><div class="collapse" id="foo"></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_action_slot_as_title_converts_to_string(): void
    {
        $view = $this->blade('<x-smc::collapse containerId="foo"><x-slot:action class="btn btn-success">Click me<i>Something</></x-slot:action></x-smc::collapse>');
        $html = $this->normalize($view);
        $expected = '<div><a href="#foo" title="Click me&lt;i&gt;Something&lt;/&gt;" class="btn btn-success" data-bs-toggle="collapse" aria-expanded="false" aria-controls="foo" data-bs-target="#foo" role="button">Click me<i>Something</></a><div class="collapse" id="foo"></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_action_with_title(): void
    {
        $view = $this->blade('<x-smc::collapse containerId="foo"><x-slot:action class="btn btn-success" title="Something">Click me<i>Something</></x-slot:action></x-smc::collapse>');
        $html = $this->normalize($view);
        $expected = '<div><a href="#foo" title="Something" class="btn btn-success" data-bs-toggle="collapse" aria-expanded="false" aria-controls="foo" data-bs-target="#foo" role="button">Click me<i>Something</></a><div class="collapse" id="foo"></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_slot(): void
    {
        // there is a weird bug where if the "slot" is directly after the</x-slot:action> it doesn't close the action slot correctly
        // and doesn't convert the action to a slot
        $view = $this->blade('<x-smc::collapse containerId="foo"><x-slot:action>Click me</x-slot:action> Slot</x-smc::collapse>');
        $html = $this->normalize($view);
        $expected = '<div><a href="#foo" title="Click me" data-bs-toggle="collapse" aria-expanded="false" aria-controls="foo" data-bs-target="#foo" role="button">Click me</a><div class="collapse" id="foo">Slot</div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_container_as_slot(): void
    {
        $view = $this->blade('<x-smc::collapse containerId="foo"><x-slot:action>Click me</x-slot:action><x-slot:container class="my-2">Slot</x-slot:container></x-smc::collapse>');
        $html = $this->normalize($view);
        $expected = '<div><a href="#foo" title="Click me" data-bs-toggle="collapse" aria-expanded="false" aria-controls="foo" data-bs-target="#foo" role="button">Click me</a><div class="collapse my-2" id="foo">Slot</div></div>';
        $this->assertEquals($expected, $html);
    }
}
