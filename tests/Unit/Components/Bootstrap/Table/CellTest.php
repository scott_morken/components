<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Table;

use Smorken\Components\Helpers\Model;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\Stubs\LabelledBackEnumStub;
use Tests\Smorken\Components\TestCase;

class CellTest extends TestCase
{
    use NormalizeString;

    public function test_default(): void
    {
        $view = $this->blade('<x-smc::table.cell></x-smc::table.cell>');
        $html = $this->normalize($view);
        $expected = '<td></td>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_model_and_slot_uses_slot_as_value(): void
    {
        $model = Model::newInstance(['key' => 'foo'], 'key');
        $view = $this->blade('<x-smc::table.cell :model="$model">Test</x-smc::table.cell>', ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<td>Test</td>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_model_as_value(): void
    {
        $model = Model::newInstance(['key' => 'foo'], 'key');
        $view = $this->blade('<x-smc::table.cell :model="$model"></x-smc::table.cell>', ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<td>foo</td>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_model_labelled_backed_enum_as_value(): void
    {
        $model = Model::newInstance(['key' => LabelledBackEnumStub::FIZ], 'key');
        $view = $this->blade('<x-smc::table.cell :model="$model"></x-smc::table.cell>', ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<td>Is Fizzy</td>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_model_boolean_as_value(): void
    {
        $model = Model::newInstance(['key' => false], 'key');
        $view = $this->blade('<x-smc::table.cell :model="$model"></x-smc::table.cell>', ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<td>No</td>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_model_array_as_value(): void
    {
        $model = Model::newInstance(['key' => ['foo' => 'bar']], 'key');
        $view = $this->blade('<x-smc::table.cell :model="$model"></x-smc::table.cell>', ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<td><pre>{ &quot;foo&quot;: &quot;bar&quot; }</pre></td>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_slot(): void
    {
        $view = $this->blade('<x-smc::table.cell>Test</x-smc::table.cell>');
        $html = $this->normalize($view);
        $expected = '<td>Test</td>';
        $this->assertEquals($expected, $html);
    }
}
