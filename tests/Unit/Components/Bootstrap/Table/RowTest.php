<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Table;

use Smorken\Data\Data;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\Stubs\ModelStub;
use Tests\Smorken\Components\TestCase;

class RowTest extends TestCase
{
    use NormalizeString;

    public function test_default(): void
    {
        $view = $this->blade('<x-smc::table.row></x-smc::table.row>');
        $html = $this->normalize($view);
        $this->assertEquals('<tr></tr>', $html);
    }

    public function test_guess_key_on_array_with_id(): void
    {
        $model = ['foo' => 'bar', 'id' => 'fiz'];
        $view = $this->blade('<x-smc::table.row :model="$model"></x-smc::table.row>', ['model' => $model]);
        $html = $this->normalize($view);
        $this->assertEquals('<tr id="row-for-fiz"></tr>', $html);
    }

    public function test_guess_key_on_array_without_id(): void
    {
        $model = ['foo' => 'bar'];
        $view = $this->blade('<x-smc::table.row :model="$model"></x-smc::table.row>', ['model' => $model]);
        $html = $this->normalize($view);
        $this->assertEquals('<tr></tr>', $html);
    }

    public function test_guess_key_on_dto_with_standard_id(): void
    {
        $model = new class(id: 1) extends Data
        {
            public function __construct(public int $id) {}
        };
        $view = $this->blade('<x-smc::table.row :model="$model"></x-smc::table.row>', ['model' => $model]);
        $html = $this->normalize($view);
        $this->assertEquals('<tr id="row-for-1"></tr>', $html);
    }

    public function test_guess_key_on_model_with_standard_id(): void
    {
        $model = (new ModelStub)->forceFill(['id' => '1']);
        $view = $this->blade('<x-smc::table.row :model="$model"></x-smc::table.row>', ['model' => $model]);
        $html = $this->normalize($view);
        $this->assertEquals('<tr id="row-for-1"></tr>', $html);
    }
}
