<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap;

use Carbon\Carbon;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;
use Smorken\Components\Components\Bootstrap\Input;
use Smorken\Components\Helpers\Model;
use Smorken\Components\Helpers\ValueConverters\Converters;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\Stubs\DataObjectStub;
use Tests\Smorken\Components\TestCase;

class InputTest extends TestCase
{
    use NormalizeString;

    public function test_blade_input(): void
    {
        $view = $this->blade('<x-smc::input/>');
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" maxlength="255" class="form-control" value=""/>', $html);
    }

    public function test_blade_input_disabled(): void
    {
        $view = $this->blade('<x-smc::input :disabled="true"/>');
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" disabled maxlength="255" class="form-control" value=""/>', $html);
    }

    public function test_blade_input_readonly(): void
    {
        $view = $this->blade('<x-smc::input :readonly="true"/>');
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" readonly maxlength="255" class="form-control" value=""/>', $html);
    }

    public function test_blade_input_with_dto_model_attribute(): void
    {
        $model = new DataObjectStub('bar');
        $view = $this->blade('<x-smc::input :model="\Smorken\Components\Helpers\Model::newInstance($model, \'foo\')"/>',
            ['model' => $model]);
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" maxlength="255" class="form-control" name="foo" id="foo" value="bar"/>',
            $html);
    }

    public function test_blade_input_with_errors(): void
    {
        $errors = [
            'test' => ['Error 1', 'Error 2'],
        ];
        $this->app['router']->get('test', [
            'middleware' => 'web', 'uses' => function () use ($errors) {
                $errorBag = new ViewErrorBag;
                $errorBag->put('default', new MessageBag($errors));
                request()->session()->put('errors', $errorBag);

                return 'test';
            },
        ]);
        $this->call('GET', 'test');
        $view = $this->withViewErrors($errors)
            ->blade('<x-smc::input name="test"/>');
        $html = $this->normalize((string) $view);
        $expected = '<input type="text" maxlength="255" class="form-control is-invalid" name="test" id="test" value=""/><div class="text-danger small">Error 1</div>';
        $this->assertEquals($expected, $html);
    }

    public function test_blade_input_with_errors_and_withErrors_false(): void
    {
        $errors = [
            'test' => ['Error 1', 'Error 2'],
        ];
        $this->app['router']->get('test', [
            'middleware' => 'web', 'uses' => function () use ($errors) {
                $errorBag = new ViewErrorBag;
                $errorBag->put('default', new MessageBag($errors));
                request()->session()->put('errors', $errorBag);

                return 'test';
            },
        ]);
        $this->call('GET', 'test');
        $view = $this->withViewErrors($errors)
            ->blade('<x-smc::input name="test" :withErrors="false"/>');
        $html = $this->normalize((string) $view);
        $expected = '<input type="text" maxlength="255" class="form-control" name="test" id="test" value=""/>';
        $this->assertEquals($expected, $html);
    }

    public function test_blade_input_with_id_and_name(): void
    {
        $view = $this->blade('<x-smc::input id="foo" name="bar[]"/>');
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" maxlength="255" class="form-control" id="foo" name="bar[]" value=""/>',
            $html);
    }

    public function test_blade_input_with_id_sets_name(): void
    {
        $view = $this->blade('<x-smc::input id="foo"/>');
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" maxlength="255" class="form-control" id="foo" name="foo" value=""/>',
            $html);
    }

    public function test_blade_input_with_model_attribute(): void
    {
        $model = [];
        $view = $this->blade('<x-smc::input :model="\Smorken\Components\Helpers\Model::newInstance($model, \'foo\')"/>',
            ['model' => $model]);
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" maxlength="255" class="form-control" name="foo" id="foo" value=""/>',
            $html);
    }

    public function test_blade_input_with_model_attribute_with_converter(): void
    {
        $model = ['d' => Carbon::parse('2020-01-01')];
        $m = (new Model($model, 'd'))->withConverter(Converters::DATE, 'm/d/Y');
        $view = $this->blade('<x-smc::input :model="$model"/>',
            ['model' => $m]);
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" maxlength="255" class="form-control" name="d" id="d" value="01/01/2020"/>',
            $html);
    }

    public function test_blade_input_with_model_attribute_with_converter_uses_default_value(): void
    {
        $model = [];
        $m = (new Model($model, 'd'))->withDefaultValue(Carbon::parse('2020-01-01'))
            ->withConverter(Converters::DATE, 'm/d/Y');
        $view = $this->blade('<x-smc::input :model="$model"/>',
            ['model' => $m]);
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" maxlength="255" class="form-control" name="d" id="d" value="01/01/2020"/>',
            $html);
    }

    public function test_blade_input_with_name_and_value_from_variable(): void
    {
        $view = $this->blade('<x-smc::input name="foo" :value="$v"/>', ['v' => 'abc']);
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" maxlength="255" class="form-control" name="foo" id="foo" value="abc"/>',
            $html);
    }

    public function test_blade_input_with_name_sets_id(): void
    {
        $view = $this->blade('<x-smc::input name="foo"/>');
        $html = $this->normalize((string) $view);
        $this->assertEquals('<input type="text" maxlength="255" class="form-control" name="foo" id="foo" value=""/>',
            $html);
    }

    public function test_component_input(): void
    {
        $view = $this->component(Input::class, ['slot' => null]);
        $this->assertEquals(false, $view->component->isCheckable());
        $this->assertEquals(false, $view->component->isMultiple());
        $this->assertEquals(null, $view->component->getId());
        $this->assertEquals(null, $view->component->getName());
    }
}
