<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Icon;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class ChevronDownTest extends TestCase
{
    use NormalizeString;

    public function test_default(): void
    {
        $view = $this->blade('<x-smc::icon.chevron-down></x-smc::icon.chevron-down>');
        $html = $this->normalize($view);
        $expected = '<svg class="bi bi-chevron-down" width="1em" height="1em" fill="currentColor" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/></svg>';
        $this->assertEquals($expected, $html);
    }

    public function test_override_attributes(): void
    {
        $view = $this->blade('<x-smc::icon.chevron-down class="fs-4 text-primary" fill="black"></x-smc::icon.chevron-down>');
        $html = $this->normalize($view);
        $expected = '<svg class="bi bi-chevron-down fs-4 text-primary" width="1em" height="1em" fill="black" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/></svg>';
        $this->assertEquals($expected, $html);
    }
}
