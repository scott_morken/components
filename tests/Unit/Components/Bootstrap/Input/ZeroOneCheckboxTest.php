<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Input;

use Smorken\Components\Helpers\Model;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class ZeroOneCheckboxTest extends TestCase
{
    use NormalizeString;

    public function test_single_checkbox(): void
    {
        $model = Model::newInstance(['hide' => 0], 'hide');
        $view = $this->blade('<x-smc::input.zero-one-checkbox label="Hide" :model="$model"></x-smc::input.zero-one-checkbox>',
            ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="mb-2"><div class="form-check "><input type="hidden" name="hide" value="0"/><input type="checkbox" class="form-check-input" name="hide" id="hide-1" value="1"/><label class="form-check-label" for="hide-1">Hide</label></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_is_checked(): void
    {
        $model = Model::newInstance(['hide' => 1], 'hide');
        $view = $this->blade('<x-smc::input.zero-one-checkbox label="Hide" :model="$model"></x-smc::input.zero-one-checkbox>',
            ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="mb-2"><div class="form-check "><input type="hidden" name="hide" value="0"/><input type="checkbox" class="form-check-input" name="hide" id="hide-1" value="1" checked/><label class="form-check-label" for="hide-1">Hide</label></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_is_checked_using_bool(): void
    {
        $model = Model::newInstance(['hide' => true], 'hide');
        $view = $this->blade('<x-smc::input.zero-one-checkbox label="Hide" :model="$model"></x-smc::input.zero-one-checkbox>',
            ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="mb-2"><div class="form-check "><input type="hidden" name="hide" value="0"/><input type="checkbox" class="form-check-input" name="hide" id="hide-1" value="1" checked/><label class="form-check-label" for="hide-1">Hide</label></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_with_name(): void
    {
        $model = Model::newInstance(['hide' => 0], 'hide');
        $name = 'something[0][hide]';
        $view = $this->blade('<x-smc::input.zero-one-checkbox label="Hide" :name="$name" :model="$model"></x-smc::input.zero-one-checkbox>',
            ['model' => $model, 'name' => $name]);
        $html = $this->normalize($view);
        $expected = '<div class="mb-2"><div class="form-check "><input type="hidden" name="something[0][hide]" value="0"/><input type="checkbox" class="form-check-input" name="something[0][hide]" id="something-0-hide-1" value="1"/><label class="form-check-label" for="something-0-hide-1">Hide</label></div></div>';
        $this->assertEquals($expected, $html);
    }
}
