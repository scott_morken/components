<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Input;

use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;
use Smorken\Components\Helpers\Checkable;
use Smorken\Components\Helpers\Model;
use Smorken\Model\VO;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class RadioTest extends TestCase
{
    use NormalizeString;

    public function test_radio_default(): void
    {
        $checkable = new Checkable('1', 'Test');
        $view = $this->blade('<x-smc::input.radio :checkable="$checkable"></x-smc::input.radio>',
            ['checkable' => $checkable]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="radio" class="form-check-input" value="1"/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_radio_disabled(): void
    {
        $checkable = new Checkable('1', 'Test');
        $view = $this->blade('<x-smc::input.radio :checkable="$checkable" :disabled="true"></x-smc::input.radio>',
            ['checkable' => $checkable]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="radio" class="form-check-input" value="1" disabled/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_radio_is_checked(): void
    {
        $checkbox = new Checkable('1', 'Test', true);
        $view = $this->blade('<x-smc::input.radio :checkable="$checkable"></x-smc::input.radio>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="radio" class="form-check-input" value="1" checked/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_radio_is_checked_with_model(): void
    {
        $model = new Model(new VO(['active' => '1']), 'active');
        $checkbox = new Checkable('1', 'Test');
        $view = $this->blade('<x-smc::input.radio :model="$model" :checkable="$checkable"></x-smc::input.radio>',
            ['checkable' => $checkbox, 'model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="radio" class="form-check-input" name="active" id="active-1" value="1" checked/><label class="form-check-label" for="active-1">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_radio_readonly(): void
    {
        $checkable = new Checkable('1', 'Test');
        $view = $this->blade('<x-smc::input.radio :checkable="$checkable" :readonly="true"></x-smc::input.radio>',
            ['checkable' => $checkable]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="radio" class="form-check-input" value="1" readonly/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_radio_with_errors(): void
    {
        $errors = [
            'test' => ['Error 1', 'Error 2'],
        ];
        $this->app['router']->get('test', [
            'middleware' => 'web', 'uses' => function () use ($errors) {
                $errorBag = new ViewErrorBag;
                $errorBag->put('default', new MessageBag($errors));
                request()->session()->put('errors', $errorBag);

                return 'test';
            },
        ]);
        $this->call('GET', 'test');
        $checkable = new Checkable('1', 'Test');
        $view = $this->withViewErrors($errors)
            ->blade('<x-smc::input.radio name="test" :checkable="$checkable"></x-smc::input.radio>',
                ['checkable' => $checkable]);
        $html = $this->normalize((string) $view);
        $expected = '<div class="form-check "><input type="radio" class="form-check-input is-invalid" name="test" id="test-1" value="1"/><label class="form-check-label" for="test-1">Test</label><span class="text-danger small">Error 1</span></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_radio_with_errors_and_withErrors_false(): void
    {
        $errors = [
            'test' => ['Error 1', 'Error 2'],
        ];
        $this->app['router']->get('test', [
            'middleware' => 'web', 'uses' => function () use ($errors) {
                $errorBag = new ViewErrorBag;
                $errorBag->put('default', new MessageBag($errors));
                request()->session()->put('errors', $errorBag);

                return 'test';
            },
        ]);
        $this->call('GET', 'test');
        $checkable = new Checkable('1', 'Test');
        $view = $this->withViewErrors($errors)
            ->blade('<x-smc::input.radio name="test" :withErrors="false" :checkable="$checkable"></x-smc::input.radio>',
                ['checkable' => $checkable]);
        $html = $this->normalize((string) $view);
        $expected = '<div class="form-check "><input type="radio" class="form-check-input" name="test" id="test-1" value="1"/><label class="form-check-label" for="test-1">Test</label></div>';
        $this->assertEquals($expected, $html);
    }
}
