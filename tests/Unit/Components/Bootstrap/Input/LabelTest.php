<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Input;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class LabelTest extends TestCase
{
    use NormalizeString;

    public function test_default(): void
    {
        $view = $this->blade('<x-smc::input.label></x-smc::input.label>');
        $html = $this->normalize($view);
        $expected = '<label for="" class="form-label"></label>';
        $this->assertEquals($expected, $html);
    }

    public function test_visually_hidden(): void
    {
        $view = $this->blade('<x-smc::input.label :visible="false">Test</x-smc::input.label>');
        $html = $this->normalize($view);
        $expected = '<label for="" class="form-label visually-hidden">Test</label>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_for(): void
    {
        $view = $this->blade('<x-smc::input.label for="foo">Test</x-smc::input.label>');
        $html = $this->normalize($view);
        $expected = '<label for="foo" class="form-label">Test</label>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_model_and_for_uses_for(): void
    {
        $model = ['id' => 'foo'];
        $view = $this->blade('<x-smc::input.label for="fiz" :model="$model">Test</x-smc::input.label>',
            ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<label for="fiz" class="form-label">Test</label>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_model_as_for(): void
    {
        $model = ['id' => 'foo'];
        $view = $this->blade('<x-smc::input.label :model="$model">Test</x-smc::input.label>', ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<label for="foo" class="form-label">Test</label>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_name_as_for(): void
    {
        $view = $this->blade('<x-smc::input.label name="foo[bar][1]">Test</x-smc::input.label>');
        $html = $this->normalize($view);
        $expected = '<label for="foo-bar-1" class="form-label">Test</label>';
        $this->assertEquals($expected, $html);
    }
}
