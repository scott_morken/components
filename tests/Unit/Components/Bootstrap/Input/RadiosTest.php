<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Input;

use Smorken\Components\Helpers\Model;
use Smorken\Model\VO;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class RadiosTest extends TestCase
{
    use NormalizeString;

    public function test_from_array_with_model(): void
    {
        $model = new Model(new VO(['foos' => 'B']), 'foos');
        $radio = [
            'A' => 'FoosA',
            'B' => 'FoosB',
            'C' => 'FoosC',
        ];
        $view = $this->blade('<x-smc::input.radios :model="$model" :radios="$radios"></x-smc::input.radios>',
            ['radios' => $radio, 'model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="radios"><div class="form-check "><input type="radio" class="form-check-input" name="foos" id="foos-a" value="A"/><label class="form-check-label" for="foos-a">FoosA</label></div><div class="form-check "><input type="radio" class="form-check-input" name="foos" id="foos-b" value="B" checked/><label class="form-check-label" for="foos-b">FoosB</label></div><div class="form-check "><input type="radio" class="form-check-input" name="foos" id="foos-c" value="C"/><label class="form-check-label" for="foos-c">FoosC</label></div></div>';
        $this->assertEquals($expected, $html);
    }
}
