<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Input;

use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;
use Smorken\Components\Helpers\Checkbox;
use Smorken\Components\Helpers\Model;
use Smorken\Model\VO;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class CheckboxTest extends TestCase
{
    use NormalizeString;

    public function test_multiple_checkbox_is_checked_with_model(): void
    {
        $model = new Model(new VO(['foos' => ['A', 'B', 'C']]), 'foos');
        $checkbox = new Checkbox('B', 'Foos', null, true);
        $view = $this->blade('<x-smc::input.checkbox :model="$model" :checkable="$checkable"></x-smc::input.checkbox>',
            ['checkable' => $checkbox, 'model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" name="foos[]" id="foos-b" value="B" checked/><label class="form-check-label" for="foos-b">Foos</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_multiple_checkbox_with_id(): void
    {
        $checkbox = new Checkbox('1', 'Test', null, true);
        $view = $this->blade('<x-smc::input.checkbox id="foo" :checkable="$checkable"></x-smc::input.checkbox>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" id="foo-1" name="foo[]" value="1"/><label class="form-check-label" for="foo-1">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_multiple_checkbox_with_name(): void
    {
        $checkbox = new Checkbox('1', 'Test', null, true);
        $view = $this->blade('<x-smc::input.checkbox name="foo" :checkable="$checkable"></x-smc::input.checkbox>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" name="foo[]" id="foo-1" value="1"/><label class="form-check-label" for="foo-1">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_multiple_checkbox_with_name_and_id(): void
    {
        $checkbox = new Checkbox('1', 'Test', null, true);
        $view = $this->blade('<x-smc::input.checkbox name="foo" id="bar" :checkable="$checkable"></x-smc::input.checkbox>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" name="foo[]" id="bar-1" value="1"/><label class="form-check-label" for="bar-1">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_multiple_checkbox_without_name(): void
    {
        $checkbox = new Checkbox('1', 'Test', null, true);
        $view = $this->blade('<x-smc::input.checkbox :checkable="$checkable"></x-smc::input.checkbox>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" value="1"/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox(): void
    {
        $checkbox = new Checkbox('1', 'Test');
        $view = $this->blade('<x-smc::input.checkbox :checkable="$checkable"></x-smc::input.checkbox>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" value="1"/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_with_errors(): void
    {
        $errors = [
            'test' => ['Error 1', 'Error 2'],
        ];
        $this->app['router']->get('test', [
            'middleware' => 'web', 'uses' => function () use ($errors) {
                $errorBag = new ViewErrorBag;
                $errorBag->put('default', new MessageBag($errors));
                request()->session()->put('errors', $errorBag);

                return 'test';
            },
        ]);
        $this->call('GET', 'test');
        $checkbox = new Checkbox('1', 'Test');
        $view = $this->withViewErrors($errors)
            ->blade('<x-smc::input.checkbox name="test" :checkable="$checkable"></x-smc::input.checkbox>',
                ['checkable' => $checkbox]);
        $html = $this->normalize((string) $view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input is-invalid" name="test" id="test-1" value="1"/><label class="form-check-label" for="test-1">Test</label><span class="text-danger small">Error 1</span></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_with_errors_and_withErrors_false(): void
    {
        $errors = [
            'test' => ['Error 1', 'Error 2'],
        ];
        $this->app['router']->get('test', [
            'middleware' => 'web', 'uses' => function () use ($errors) {
                $errorBag = new ViewErrorBag;
                $errorBag->put('default', new MessageBag($errors));
                request()->session()->put('errors', $errorBag);

                return 'test';
            },
        ]);
        $this->call('GET', 'test');
        $checkbox = new Checkbox('1', 'Test');
        $view = $this->withViewErrors($errors)
            ->blade('<x-smc::input.checkbox name="test" :withErrors="false" :checkable="$checkable"></x-smc::input.checkbox>',
                ['checkable' => $checkbox]);
        $html = $this->normalize((string) $view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" name="test" id="test-1" value="1"/><label class="form-check-label" for="test-1">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_readonly(): void
    {
        $checkbox = new Checkbox('1', 'Test');
        $view = $this->blade('<x-smc::input.checkbox :checkable="$checkable" :readonly="true"></x-smc::input.checkbox>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" value="1" readonly/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_disabled(): void
    {
        $checkbox = new Checkbox('1', 'Test');
        $view = $this->blade('<x-smc::input.checkbox :checkable="$checkable" :disabled="true"></x-smc::input.checkbox>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" value="1" disabled/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_is_checked(): void
    {
        $checkbox = new Checkbox('1', 'Test', true);
        $view = $this->blade('<x-smc::input.checkbox :checkable="$checkable"></x-smc::input.checkbox>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" value="1" checked/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_is_checked_with_model(): void
    {
        $model = new Model(new VO(['active' => '1']), 'active');
        $checkbox = new Checkbox('1', 'Test');
        $view = $this->blade('<x-smc::input.checkbox :model="$model" :checkable="$checkable"></x-smc::input.checkbox>',
            ['checkable' => $checkbox, 'model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="checkbox" class="form-check-input" name="active" id="active-1" value="1" checked/><label class="form-check-label" for="active-1">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_is_not_checked_with_model_and_hidden(): void
    {
        $model = new Model(new VO(['active' => '0']), 'active');
        $checkbox = new Checkbox('1', 'Test', null, false, '0');
        $view = $this->blade('<x-smc::input.checkbox :model="$model" :checkable="$checkable"></x-smc::input.checkbox>',
            ['checkable' => $checkbox, 'model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="hidden" name="active" value="0"/><input type="checkbox" class="form-check-input" name="active" id="active-1" value="1"/><label class="form-check-label" for="active-1">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_with_hidden_value(): void
    {
        $checkbox = new Checkbox('1', 'Test', null, false, '0');
        $view = $this->blade('<x-smc::input.checkbox :checkable="$checkable"></x-smc::input.checkbox>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="hidden" value="0"/><input type="checkbox" class="form-check-input" value="1"/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_single_checkbox_with_hidden_value_is_disabled(): void
    {
        $checkbox = new Checkbox('1', 'Test', null, false, '0');
        $view = $this->blade('<x-smc::input.checkbox :checkable="$checkable" :disabled="true"></x-smc::input.checkbox>',
            ['checkable' => $checkbox]);
        $html = $this->normalize($view);
        $expected = '<div class="form-check "><input type="hidden" disabled value="0"/><input type="checkbox" class="form-check-input" value="1" disabled/><label class="form-check-label" for="">Test</label></div>';
        $this->assertEquals($expected, $html);
    }
}
