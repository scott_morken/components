<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Input;

use Smorken\Components\Helpers\Model;
use Smorken\Components\Helpers\Option;
use Smorken\Model\VO;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class SelectOptionsTest extends TestCase
{
    use NormalizeString;

    public function test_default_disabled(): void
    {
        $view = $this->blade('<x-smc::input.select-options :options="$options" :disabled="true"></x-smc::input.select-options>',
            ['options' => []]);
        $html = $this->normalize($view);
        $expected = '<select class="form-select" disabled></select>';
        $this->assertEquals($expected, $html);
    }

    public function test_default(): void
    {
        $view = $this->blade('<x-smc::input.select-options :options="$options"></x-smc::input.select-options>',
            ['options' => []]);
        $html = $this->normalize($view);
        $expected = '<select class="form-select"></select>';
        $this->assertEquals($expected, $html);
    }

    public function test_default_with_options_array(): void
    {
        $view = $this->blade('<x-smc::input.select-options :options="$options"></x-smc::input.select-options>',
            ['options' => ['foo' => 'Bar', 'fiz' => 'Buzz']]);
        $html = $this->normalize($view);
        $expected = '<select class="form-select"><option value="foo">Bar</option><option value="fiz">Buzz</option></select>';
        $this->assertEquals($expected, $html);
    }

    public function test_default_with_options_array_and_model_with_selected(): void
    {
        $model = new Model(new VO(['thing' => 'fiz']), 'thing');
        $view = $this->blade('<x-smc::input.select-options :model="$model" :options="$options"></x-smc::input.select-options>',
            ['options' => ['foo' => 'Bar', 'fiz' => 'Buzz'], 'model' => $model]);
        $html = $this->normalize($view);
        $expected = '<select class="form-select" name="thing" id="thing"><option value="foo">Bar</option><option value="fiz" selected>Buzz</option></select>';
        $this->assertEquals($expected, $html);
    }

    public function test_default_with_options_array_and_model_with_default_value_selected(): void
    {
        $model = new Model(new VO(['thing' => '']), 'thing');
        $model->withDefaultValue('fiz');
        $view = $this->blade('<x-smc::input.select-options :model="$model" :options="$options"></x-smc::input.select-options>',
            ['options' => ['foo' => 'Bar', 'fiz' => 'Buzz'], 'model' => $model]);
        $html = $this->normalize($view);
        $expected = '<select class="form-select" name="thing" id="thing"><option value="foo">Bar</option><option value="fiz" selected>Buzz</option></select>';
        $this->assertEquals($expected, $html);
    }

    public function test_default_with_options_objects(): void
    {
        $view = $this->blade('<x-smc::input.select-options :options="$options"></x-smc::input.select-options>',
            [
                'options' => [
                    new Option('foo', 'Bar'),
                    new Option('fiz', 'Buzz', true),
                ],
            ]);
        $html = $this->normalize($view);
        $expected = '<select class="form-select"><option value="foo">Bar</option><option value="fiz" selected>Buzz</option></select>';
        $this->assertEquals($expected, $html);
    }

    public function test_default_with_options_objects_and_disabled(): void
    {
        $view = $this->blade('<x-smc::input.select-options :options="$options"></x-smc::input.select-options>',
            [
                'options' => [
                    new Option('foo', 'Bar', false, true),
                    new Option('fiz', 'Buzz', true),
                ],
            ]);
        $html = $this->normalize($view);
        $expected = '<select class="form-select"><option value="foo" disabled>Bar</option><option value="fiz" selected>Buzz</option></select>';
        $this->assertEquals($expected, $html);
    }
}
