<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Input;

use Smorken\Components\Helpers\Model;
use Smorken\Model\VO;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class TextAreaTest extends TestCase
{
    use NormalizeString;

    public function test_defaults(): void
    {
        $view = $this->blade('<x-smc::input.text-area></x-smc::input.text-area>');
        $html = $this->normalize($view);
        $expected = '<textarea rows="3" maxlength="4096" class="form-control"></textarea>';
        $this->assertEquals($expected, $html);
    }

    public function test_value_from_model(): void
    {
        $model = new Model(new VO(['html' => '<div>Foo</div>']), 'html');
        $view = $this->blade('<x-smc::input.text-area :model="$model"></x-smc::input.text-area>', ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<textarea rows="3" maxlength="4096" class="form-control" name="html" id="html">&lt;div&gt;Foo&lt;/div&gt;</textarea>';
        $this->assertEquals($expected, $html);
    }

    public function test_value_on_slot(): void
    {
        $view = $this->blade('<x-smc::input.text-area><div>Test</div></x-smc::input.text-area>');
        $html = $this->normalize($view);
        $expected = '<textarea rows="3" maxlength="4096" class="form-control"><div>Test</div></textarea>';
        $this->assertEquals($expected, $html);
    }
}
