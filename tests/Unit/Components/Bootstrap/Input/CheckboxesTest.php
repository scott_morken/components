<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Input;

use Smorken\Components\Helpers\Model;
use Smorken\Model\VO;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class CheckboxesTest extends TestCase
{
    use NormalizeString;

    public function test_from_array_with_model_multiple_checked(): void
    {
        $model = new Model(new VO(['foos' => ['A', 'C']]), 'foos');
        $checkboxes = [
            'A' => 'FoosA',
            'B' => 'FoosB',
            'C' => 'FoosC',
        ];
        $view = $this->blade('<x-smc::input.checkboxes :model="$model" :checkboxes="$checkboxes"></x-smc::input.checkboxes>',
            ['checkboxes' => $checkboxes, 'model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="checkboxes"><div class="form-check "><input type="checkbox" class="form-check-input" name="foos[]" id="foos-a" value="A" checked/><label class="form-check-label" for="foos-a">FoosA</label></div><div class="form-check "><input type="checkbox" class="form-check-input" name="foos[]" id="foos-b" value="B"/><label class="form-check-label" for="foos-b">FoosB</label></div><div class="form-check "><input type="checkbox" class="form-check-input" name="foos[]" id="foos-c" value="C" checked/><label class="form-check-label" for="foos-c">FoosC</label></div></div>';
        $this->assertEquals($expected, $html);
    }

    public function test_old_from_array(): void
    {
        $this->app['router']->get('test', [
            'middleware' => 'web', 'uses' => function () {
                $request = request()->merge(['foos' => ['A', 'C']]);
                $request->flash();

                return 'test';
            },
        ]);
        $this->call('GET', 'test');
        $model = new Model(new VO(['foos' => []]), 'foos');
        $checkboxes = [
            'A' => 'FoosA',
            'B' => 'FoosB',
            'C' => 'FoosC',
        ];
        $view = $this->blade('<x-smc::input.checkboxes :model="$model" :checkboxes="$checkboxes"></x-smc::input.checkboxes>',
            ['checkboxes' => $checkboxes, 'model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="checkboxes"><div class="form-check "><input type="checkbox" class="form-check-input" name="foos[]" id="foos-a" value="A" checked/><label class="form-check-label" for="foos-a">FoosA</label></div><div class="form-check "><input type="checkbox" class="form-check-input" name="foos[]" id="foos-b" value="B"/><label class="form-check-label" for="foos-b">FoosB</label></div><div class="form-check "><input type="checkbox" class="form-check-input" name="foos[]" id="foos-c" value="C" checked/><label class="form-check-label" for="foos-c">FoosC</label></div></div>';
        $this->assertEquals($expected, $html);
    }
}
