<?php

namespace Tests\Smorken\Components\Unit\Components\Bootstrap;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class LinkTest extends TestCase
{
    use NormalizeString;

    public function test_default(): void
    {
        $view = $this->blade('<x-smc::link></x-smc::link>');
        $html = $this->normalize($view);
        $expected = '<a href="#" title="#">#</a>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_href(): void
    {
        $view = $this->blade('<x-smc::link href="/foo"></x-smc::link>');
        $html = $this->normalize($view);
        $expected = '<a href="/foo" title="/foo">/foo</a>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_href_and_title_and_slot(): void
    {
        $view = $this->blade('<x-smc::link href="/foo" title="Foo">Test</x-smc::link>');
        $html = $this->normalize($view);
        $expected = '<a href="/foo" title="Foo">Test</a>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_slot(): void
    {
        $view = $this->blade('<x-smc::link>Test</x-smc::link>');
        $html = $this->normalize($view);
        $expected = '<a href="#" title="Test">Test</a>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_title(): void
    {
        $view = $this->blade('<x-smc::link title="Test"></x-smc::link>');
        $html = $this->normalize($view);
        $expected = '<a href="#" title="Test">Test</a>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_title_and_slot(): void
    {
        $view = $this->blade('<x-smc::link title="Foo">Test</x-smc::link>');
        $html = $this->normalize($view);
        $expected = '<a href="#" title="Foo">Test</a>';
        $this->assertEquals($expected, $html);
    }
}
