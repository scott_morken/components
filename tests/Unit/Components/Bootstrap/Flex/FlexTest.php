<?php
declare(strict_types=1);

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Flex;

use PHPUnit\Framework\Attributes\Test;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class FlexTest extends TestCase
{
    use NormalizeString;

    #[Test]
    public function it_should_create_a_default_flex_container(): void
    {
        $view = $this->blade('<x-smc::flex></x-smc::flex>');
        $html = $this->normalize((string) $view);
        $expected = '<div class="d-flex gap-3 flex-column flex-md-row flex-md-wrap"></div>';
        $this->assertEquals($expected, $html);
    }

    #[Test]
    public function it_should_create_a_flex_container_with_justification(): void
    {
        $view = $this->blade('<x-smc::flex class="justify-content-start"></x-smc::flex>');
        $html = $this->normalize((string) $view);
        $expected = '<div class="d-flex gap-3 flex-column flex-md-row flex-md-wrap justify-content-start"></div>';
        $this->assertEquals($expected, $html);
    }

    #[Test]
    public function it_should_create_a_justify_between_flex_container(): void
    {
        $view = $this->blade('<x-smc::flex.between></x-smc::flex.between>');
        $html = $this->normalize((string) $view);
        $expected = '<div class="d-flex gap-3 flex-column flex-md-row flex-md-wrap justify-content-between"></div>';
        $this->assertEquals($expected, $html);
    }
}