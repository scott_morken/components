<?php

declare(strict_types=1);

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Resource;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Domain\ViewModels\RetrieveViewModel;
use Smorken\Model\Eloquent;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class ShowRowsTest extends TestCase
{
    use NormalizeString;

    #[Test]
    public function it_shows_a_friendly_value_for_a_boolean(): void
    {
        $model = new class(['id' => 1, 'foo' => true]) extends Eloquent
        {
            protected $fillable = ['id', 'foo'];
        };
        $vm = new RetrieveViewModel($model);
        $view = (string) $this->blade('<x-smc::resource.show-rows :viewModel="$vm" :actions="false"></x-smc::resource.part.show-attribute-rows>', ['vm' => $vm]);
        $html = $this->normalize($view);
        $expected = '<h5 class="mb-2">View record [1]</h5><div class="row mb-2"><div class="col-sm-3 col-md-2 fw-bold">ID</div><div class="col-sm-9 col-md-10"> 1</div></div><div class="row mb-2"><div class="col-sm-3 col-md-2 fw-bold">Foo</div><div class="col-sm-9 col-md-10"> Yes</div></div>';
        $this->assertEquals($expected, $html);
    }
}
