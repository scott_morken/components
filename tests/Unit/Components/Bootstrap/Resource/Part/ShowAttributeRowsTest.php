<?php

declare(strict_types=1);

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Resource\Part;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Model\Eloquent;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class ShowAttributeRowsTest extends TestCase
{
    use NormalizeString;

    #[Test]
    public function it_shows_friendly_value_for_booleans(): void
    {
        $model = new class(['foo' => 1]) extends Eloquent
        {
            protected $fillable = ['foo'];

            protected function casts(): array
            {
                return [
                    'foo' => 'boolean',
                ];
            }
        };
        $view = (string) $this->blade('<x-smc::resource.part.show-attribute-rows :model="$model"></x-smc::resource.part.show-attribute-rows>', ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="row mb-2"><div class="col-sm-3 col-md-2 fw-bold">Foo</div><div class="col-sm-9 col-md-10"> Yes</div></div>';
        $this->assertEquals($expected, $html);
    }

    #[Test]
    public function it_shows_friendly_value_for_carbon(): void
    {
        $model = new class(['foo' => '2022-01-01']) extends Eloquent
        {
            protected $fillable = ['foo'];

            protected function casts(): array
            {
                return [
                    'foo' => 'date',
                ];
            }
        };
        $view = (string) $this->blade('<x-smc::resource.part.show-attribute-rows :model="$model"></x-smc::resource.part.show-attribute-rows>', ['model' => $model]);
        $html = $this->normalize($view);
        $expected = '<div class="row mb-2"><div class="col-sm-3 col-md-2 fw-bold">Foo</div><div class="col-sm-9 col-md-10"> Sat, Jan 1, 2022 12:00 AM</div></div>';
        $this->assertEquals($expected, $html);
    }
}
