<?php

declare(strict_types=1);

namespace Tests\Smorken\Components\Unit\Components\Bootstrap\Resource\Part;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class ShowRowTest extends TestCase
{
    use NormalizeString;

    #[Test]
    public function it_should_create_a_default_row(): void
    {
        $view = $this->blade('<x-smc::resource.part.show-row attribute="foo"></x-smc::resource.part.show-row>');
        $html = $this->normalize((string) $view);
        $expected = '<div class="row mb-2"><div class="col-sm-3 col-md-2 fw-bold">foo</div><div class="col-sm-9 col-md-10"></div></div>';
        $this->assertEquals($expected, $html);
    }

    #[Test]
    public function it_should_create_a_row_with_an_array_value(): void
    {
        $view = $this->blade('<x-smc::resource.part.show-row attribute="foo" :value="[\'foo\' => \'bar\']"></x-smc::resource.part.show-row>');
        $html = $this->normalize((string) $view);
        $expected = '<div class="row mb-2"><div class="col-sm-3 col-md-2 fw-bold">foo</div><div class="col-sm-9 col-md-10"><pre>{ &quot;foo&quot;: &quot;bar&quot; }</pre></div></div>';
        $this->assertEquals($expected, $html);
    }

    #[Test]
    public function it_should_create_a_row_with_a_carbon_value(): void
    {
        $date = Carbon::parse('2022-01-01');
        $view = $this->blade('<x-smc::resource.part.show-row attribute="foo" :value="$value"></x-smc::resource.part.show-row>', ['value' => $date]);
        $html = $this->normalize((string) $view);
        $expected = '<div class="row mb-2"><div class="col-sm-3 col-md-2 fw-bold">foo</div><div class="col-sm-9 col-md-10"> Sat, Jan 1, 2022 12:00 AM</div></div>';
        $this->assertEquals($expected, $html);
    }

    #[Test]
    public function it_should_create_a_default_row_with_a_slot(): void
    {
        $view = $this->blade('<x-smc::resource.part.show-row attribute="foo">bar</x-smc::resource.part.show-row>');
        $html = $this->normalize((string) $view);
        $expected = '<div class="row mb-2"><div class="col-sm-3 col-md-2 fw-bold">foo</div><div class="col-sm-9 col-md-10"> bar</div></div>';
        $this->assertEquals($expected, $html);
    }
}
