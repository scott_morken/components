<?php

namespace Tests\Smorken\Components\Unit\Components\Layouts;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class NoNavTest extends TestCase
{
    use NormalizeString;

    public function test_defaults(): void
    {
        $view = $this->blade('<x-smc::layouts.no-nav></x-smc::layouts.no-nav>');
        $html = $this->normalize($view);
        $expected = '<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><!-- CSRF Token --><meta name="csrf-token" content=""><title>Laravel</title><link rel="icon" href="http://localhost/favicon.ico" sizes="16x16 32x32 48x48 64x64" type="image/vnd.microsoft.icon"><!-- Scripts --><!-- Styles --><link href="http://localhost/css/app.css" rel="stylesheet"></head><body><div id="app"><div id="ajax-loading" class="spinner-border text-warning" role="status"><span class="visually-hidden">Loading...</span></div><header><nav class="navbar navbar-expand-md navbar-dark shadow-sm bg-brand"><div class="container"><a class="navbar-brand" href="http://localhost"><img src="http://localhost/images/logo.png" alt="Logo"> Laravel</a></div></nav><div id="messages"></div></header><main class="py-4 container"></main><footer class="py-4 container"><div class="text-center"><div class="text-muted"><small>&copy; 2025 Phoenix College</small><div><a href="https://www.maricopa.edu" target="_blank" title="Maricopa Community Colleges" rel="noopener"><img src="http://localhost/images/footer.png" alt="A Maricopa Community College" height="30"></a></div></div></div></footer></div><script src="http://localhost/js/app.js"></script></body></html>';
        $this->assertEquals($expected, $html);
    }

    public function test_fluid_container(): void
    {
        $view = $this->blade('<x-smc::layouts.no-nav :isFluid="true"></x-smc::layouts.no-nav>');
        $html = $this->normalize($view);
        $expected = '<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><!-- CSRF Token --><meta name="csrf-token" content=""><title>Laravel</title><link rel="icon" href="http://localhost/favicon.ico" sizes="16x16 32x32 48x48 64x64" type="image/vnd.microsoft.icon"><!-- Scripts --><!-- Styles --><link href="http://localhost/css/app.css" rel="stylesheet"></head><body><div id="app"><div id="ajax-loading" class="spinner-border text-warning" role="status"><span class="visually-hidden">Loading...</span></div><header><nav class="navbar navbar-expand-md navbar-dark shadow-sm bg-brand"><div class="container-fluid"><a class="navbar-brand" href="http://localhost"><img src="http://localhost/images/logo.png" alt="Logo"> Laravel</a></div></nav><div id="messages"></div></header><main class="py-4 container-fluid"></main><footer class="py-4 container-fluid"><div class="text-center"><div class="text-muted"><small>&copy; 2025 Phoenix College</small><div><a href="https://www.maricopa.edu" target="_blank" title="Maricopa Community Colleges" rel="noopener"><img src="http://localhost/images/footer.png" alt="A Maricopa Community College" height="30"></a></div></div></div></footer></div><script src="http://localhost/js/app.js"></script></body></html>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_header_slot(): void
    {
        $view = $this->blade('<x-smc::layouts.no-nav><x-slot:header><div>Header</div></x-slot:header></x-smc::layouts.no-nav>');
        $html = $this->normalize($view);
        $expected = '<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><!-- CSRF Token --><meta name="csrf-token" content=""><title>Laravel</title><link rel="icon" href="http://localhost/favicon.ico" sizes="16x16 32x32 48x48 64x64" type="image/vnd.microsoft.icon"><!-- Scripts --><!-- Styles --><link href="http://localhost/css/app.css" rel="stylesheet"></head><body><div id="app"><div id="ajax-loading" class="spinner-border text-warning" role="status"><span class="visually-hidden">Loading...</span></div><header><div>Header</div></header><main class="py-4 container"></main><footer class="py-4 container"><div class="text-center"><div class="text-muted"><small>&copy; 2025 Phoenix College</small><div><a href="https://www.maricopa.edu" target="_blank" title="Maricopa Community Colleges" rel="noopener"><img src="http://localhost/images/footer.png" alt="A Maricopa Community College" height="30"></a></div></div></div></footer></div><script src="http://localhost/js/app.js"></script></body></html>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_footer_slot(): void
    {
        $view = $this->blade('<x-smc::layouts.no-nav><x-slot:footer><div>Footer</div></x-slot:footer></x-smc::layouts.no-nav>');
        $html = $this->normalize($view);
        $expected = '<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><!-- CSRF Token --><meta name="csrf-token" content=""><title>Laravel</title><link rel="icon" href="http://localhost/favicon.ico" sizes="16x16 32x32 48x48 64x64" type="image/vnd.microsoft.icon"><!-- Scripts --><!-- Styles --><link href="http://localhost/css/app.css" rel="stylesheet"></head><body><div id="app"><div id="ajax-loading" class="spinner-border text-warning" role="status"><span class="visually-hidden">Loading...</span></div><header><nav class="navbar navbar-expand-md navbar-dark shadow-sm bg-brand"><div class="container"><a class="navbar-brand" href="http://localhost"><img src="http://localhost/images/logo.png" alt="Logo"> Laravel</a></div></nav><div id="messages"></div></header><main class="py-4 container"></main><footer class="py-4 container"><div>Footer</div></footer></div><script src="http://localhost/js/app.js"></script></body></html>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_title_slot(): void
    {
        $view = $this->blade('<x-smc::layouts.no-nav><x-slot:title>Foo</x-slot:title></x-smc::layouts.no-nav>');
        $html = $this->normalize($view);
        $expected = '<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><!-- CSRF Token --><meta name="csrf-token" content=""><title>Foo</title><link rel="icon" href="http://localhost/favicon.ico" sizes="16x16 32x32 48x48 64x64" type="image/vnd.microsoft.icon"><!-- Scripts --><!-- Styles --><link href="http://localhost/css/app.css" rel="stylesheet"></head><body><div id="app"><div id="ajax-loading" class="spinner-border text-warning" role="status"><span class="visually-hidden">Loading...</span></div><header><nav class="navbar navbar-expand-md navbar-dark shadow-sm bg-brand"><div class="container"><a class="navbar-brand" href="http://localhost"><img src="http://localhost/images/logo.png" alt="Logo"> Laravel</a></div></nav><div id="messages"></div></header><main class="py-4 container"></main><footer class="py-4 container"><div class="text-center"><div class="text-muted"><small>&copy; 2025 Phoenix College</small><div><a href="https://www.maricopa.edu" target="_blank" title="Maricopa Community Colleges" rel="noopener"><img src="http://localhost/images/footer.png" alt="A Maricopa Community College" height="30"></a></div></div></div></footer></div><script src="http://localhost/js/app.js"></script></body></html>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_scripts_head_slot(): void
    {
        $view = $this->blade('<x-smc::layouts.no-nav><x-slot:scripts-head><script src="{{ asset(\'foo.js\') }}"></script></x-slot:scripts-head></x-smc::layouts.no-nav>');
        $html = $this->normalize($view);
        $expected = '<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><!-- CSRF Token --><meta name="csrf-token" content=""><title>Laravel</title><link rel="icon" href="http://localhost/favicon.ico" sizes="16x16 32x32 48x48 64x64" type="image/vnd.microsoft.icon"><!-- Scripts --><script src="http://localhost/foo.js"></script><!-- Styles --><link href="http://localhost/css/app.css" rel="stylesheet"></head><body><div id="app"><div id="ajax-loading" class="spinner-border text-warning" role="status"><span class="visually-hidden">Loading...</span></div><header><nav class="navbar navbar-expand-md navbar-dark shadow-sm bg-brand"><div class="container"><a class="navbar-brand" href="http://localhost"><img src="http://localhost/images/logo.png" alt="Logo"> Laravel</a></div></nav><div id="messages"></div></header><main class="py-4 container"></main><footer class="py-4 container"><div class="text-center"><div class="text-muted"><small>&copy; 2025 Phoenix College</small><div><a href="https://www.maricopa.edu" target="_blank" title="Maricopa Community Colleges" rel="noopener"><img src="http://localhost/images/footer.png" alt="A Maricopa Community College" height="30"></a></div></div></div></footer></div><script src="http://localhost/js/app.js"></script></body></html>';
        $this->assertEquals($expected, $html);
    }
}
