<?php

namespace Tests\Smorken\Components\Unit\Components\Layouts;

use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\TestCase;

class AppTest extends TestCase
{
    use NormalizeString;

    public function test_defaults(): void
    {
        $view = $this->blade('<x-smc::layouts.app></x-smc::layouts.app>');
        $html = $this->normalize($view);
        $expected = '<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><!-- CSRF Token --><meta name="csrf-token" content=""><title>Laravel</title><link rel="icon" href="http://localhost/favicon.ico" sizes="16x16 32x32 48x48 64x64" type="image/vnd.microsoft.icon"><!-- Scripts --><!-- Styles --><link href="http://localhost/css/app.css" rel="stylesheet"></head><body><div id="app"><div id="ajax-loading" class="spinner-border text-warning" role="status"><span class="visually-hidden">Loading...</span></div><header><nav class="navbar navbar-expand-md navbar-dark shadow-sm bg-brand"><div class="container"><a class="navbar-brand" href="http://localhost"><img src="http://localhost/images/logo.png" alt="Logo"> Laravel</a><button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button><div class="collapse navbar-collapse" id="navbarSupportedContent"><!-- Left Side Of Navbar --><ul class="navbar-nav me-auto"></ul><!-- Right Side Of Navbar --><ul class="navbar-nav justify-content-right"><!-- Authentication Links --><li class="nav-item"><a class="nav-link" href="http://localhost/login">Login</a></li></ul></div></div></nav><div id="messages"></div></header><main class="py-4 container"></main><footer class="py-4 container"><div class="text-center"><div class="text-muted"><small>&copy; 2025 Phoenix College</small><div><a href="https://www.maricopa.edu" target="_blank" title="Maricopa Community Colleges" rel="noopener"><img src="http://localhost/images/footer.png" alt="A Maricopa Community College" height="30"></a></div></div></div></footer></div><script src="http://localhost/js/app.js"></script></body></html>';
        $this->assertEquals($expected, $html);
    }

    public function test_with_slot(): void
    {
        $view = $this->blade('<x-smc::layouts.app><x-smc::title>My Page</x-smc::title></x-smc::layouts.app>');
        $html = $this->normalize($view);
        $expected = '<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><!-- CSRF Token --><meta name="csrf-token" content=""><title>Laravel</title><link rel="icon" href="http://localhost/favicon.ico" sizes="16x16 32x32 48x48 64x64" type="image/vnd.microsoft.icon"><!-- Scripts --><!-- Styles --><link href="http://localhost/css/app.css" rel="stylesheet"></head><body><div id="app"><div id="ajax-loading" class="spinner-border text-warning" role="status"><span class="visually-hidden">Loading...</span></div><header><nav class="navbar navbar-expand-md navbar-dark shadow-sm bg-brand"><div class="container"><a class="navbar-brand" href="http://localhost"><img src="http://localhost/images/logo.png" alt="Logo"> Laravel</a><button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button><div class="collapse navbar-collapse" id="navbarSupportedContent"><!-- Left Side Of Navbar --><ul class="navbar-nav me-auto"></ul><!-- Right Side Of Navbar --><ul class="navbar-nav justify-content-right"><!-- Authentication Links --><li class="nav-item"><a class="nav-link" href="http://localhost/login">Login</a></li></ul></div></div></nav><div id="messages"></div></header><main class="py-4 container"><h1>My Page</h1></main><footer class="py-4 container"><div class="text-center"><div class="text-muted"><small>&copy; 2025 Phoenix College</small><div><a href="https://www.maricopa.edu" target="_blank" title="Maricopa Community Colleges" rel="noopener"><img src="http://localhost/images/footer.png" alt="A Maricopa Community College" height="30"></a></div></div></div></footer></div><script src="http://localhost/js/app.js"></script></body></html>';
        $this->assertEquals($expected, $html);
    }
}
