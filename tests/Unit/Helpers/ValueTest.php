<?php

namespace Tests\Smorken\Components\Unit\Helpers;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Components\Helpers\Value;
use Smorken\Components\Helpers\ValueConverters\Converters;
use Tests\Smorken\Components\Concerns\NormalizeString;
use Tests\Smorken\Components\Stubs\BackedEnumStub;
use Tests\Smorken\Components\Stubs\LabelledBackEnumStub;
use Tests\Smorken\Components\Stubs\UnitEnumStub;

class ValueTest extends TestCase
{
    use NormalizeString;

    #[Test]
    public function it_uses_a_provided_converter(): void
    {
        $sut = (new Value(Carbon::parse('2022-01-01'), false))
            ->withConverter(Converters::DATE, 'm/d/y');
        $this->assertEquals('01/01/22', (string) $sut);
    }

    public function test_array_value(): void
    {
        $sut = new Value(['foo' => 'bar', 'biz' => 'buz']);
        $this->assertEquals('bar-buz', (string) $sut);
    }

    public function test_array_value_friendly(): void
    {
        $sut = new Value(['foo' => 'bar', 'biz' => 'buz'], true);
        $this->assertEquals('{ "foo": "bar", "biz": "buz" }', $this->normalize((string) $sut));
    }

    public function test_array_value_friendly_htmlable(): void
    {
        $sut = new Value(['foo' => 'bar', 'biz' => 'buz'], true);
        $this->assertEquals('<pre>{ &quot;foo&quot;: &quot;bar&quot;, &quot;biz&quot;: &quot;buz&quot; }</pre>',
            $this->normalize((string) $sut->htmlable()));
    }

    public function test_backed_enum_value(): void
    {
        $sut = new Value(BackedEnumStub::FIZ);
        $this->assertEquals('fizzy', (string) $sut);
    }

    public function test_boolean_values(): void
    {
        $sut = new Value(true);
        $this->assertEquals('1', (string) $sut);
        $sut = new Value(false);
        $this->assertEquals('0', (string) $sut);
    }

    public function test_carbon_value(): void
    {
        $sut = new Value(Carbon::parse('2022-01-01'));
        $this->assertEquals('2022-01-01 00:00:00', (string) $sut);
    }

    public function test_date_time_value(): void
    {
        $sut = new Value(\DateTime::createFromFormat('Y-m-d H:i:s', '2022-01-01 00:00:00'));
        $this->assertEquals('2022-01-01 00:00:00', (string) $sut);
    }

    public function test_empty_array_value_with_default(): void
    {
        $sut = new Value([]);
        $sut->withDefaultValue(['foo' => 'bar']);
        $this->assertEquals('bar', (string) $sut);
    }

    public function test_empty_string_value_with_default(): void
    {
        $sut = new Value('');
        $sut->withDefaultValue('foo');
        $this->assertEquals('foo', (string) $sut);
    }

    public function test_friendly_boolean_values(): void
    {
        $sut = new Value(true, true);
        $this->assertEquals('Yes', (string) $sut);
        $sut = new Value(false, true);
        $this->assertEquals('No', (string) $sut);
    }

    public function test_friendly_carbon_value(): void
    {
        $sut = new Value(Carbon::parse('2022-01-01'), true);
        $this->assertEquals('Sat, Jan 1, 2022 12:00 AM', (string) $sut);
    }

    public function test_friendly_carbon_value_htmlable(): void
    {
        $sut = new Value(Carbon::parse('2022-01-01'), true);
        $this->assertEquals('Sat, Jan 1, 2022 12:00 AM', (string) $sut->htmlable());
    }

    public function test_labelled_backed_enum_friendly_value(): void
    {
        $sut = new Value(LabelledBackEnumStub::FIZ, true);
        $this->assertEquals('Is Fizzy', (string) $sut);
    }

    public function test_labelled_backed_enum_value(): void
    {
        $sut = new Value(LabelledBackEnumStub::FIZ);
        $this->assertEquals('fizzy', (string) $sut);
    }

    public function test_null_value(): void
    {
        $sut = new Value(null);
        $this->assertEquals('', (string) $sut);
    }

    public function test_null_value_with_default(): void
    {
        $sut = new Value(null);
        $sut->withDefaultValue('foo');
        $this->assertEquals('foo', (string) $sut);
    }

    public function test_unit_enum_value(): void
    {
        $sut = new Value(UnitEnumStub::FOO);
        $this->assertEquals('foo', (string) $sut);
    }

    public function test_via_callback_using_carbon(): void
    {
        $sut = new Value(Carbon::parse('2022-01-01'));
        $this->assertEquals('2022-01-01', $sut->viaCallback(fn(Carbon $value) => $value->format('Y-m-d')));
    }

    public function test_zero_value(): void
    {
        $sut = new Value(0);
        $this->assertEquals('0', (string) $sut);
    }
}
