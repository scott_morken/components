<?php
declare(strict_types=1);

namespace Tests\Smorken\Components\Unit\Helpers\ValueConverters;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Components\Helpers\Value;
use Smorken\Components\Helpers\ValueConverters\DateTimeConverter;

class DateTimeConverterTest extends TestCase
{

    #[Test]
    public function it_applies_a_format_to_a_datetime_instance(): void
    {
        $value = new \DateTime('2022-01-01 12:00:00');
        $sut = new DateTimeConverter('Y-m-d h:i A');
        $this->assertEquals('2022-01-01 12:00 PM', $sut->convert($value, true));
    }

    #[Test]
    public function it_applies_a_named_arg_format_to_a_datetime_instance(): void
    {
        $value = new \DateTime('2022-01-01 12:00:00');
        $sut = new DateTimeConverter(format: 'Y-m-d h:i A');
        $this->assertEquals('2022-01-01 12:00 PM', $sut->convert($value, false));
    }

    #[Test]
    public function it_should_convert_a_carbon_instance(): void
    {
        $value = Carbon::parse('2022-01-01 12:00:00');
        $sut = new DateTimeConverter();
        $this->assertEquals('2022-01-01 12:00:00', $sut->convert($value, false));
    }

    #[Test]
    public function it_should_convert_a_datetime_instance(): void
    {
        $value = new \DateTime('2022-01-01 12:00:00');
        $sut = new DateTimeConverter();
        $this->assertEquals('2022-01-01 12:00:00', $sut->convert($value, true));
    }
}
