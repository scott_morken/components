<?php

namespace Tests\Smorken\Components\Unit\Helpers;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Components\Helpers\Model;

class ModelTest extends TestCase
{
    #[Test]
    public function it_can_use_a_callback(): void
    {
        $model = ['start' => Carbon::parse('2022-01-01')];
        $sut = new Model($model, 'start', fn (Carbon $start): string => $start->format('Y-m-d').' foo');
        $this->assertInstanceOf(Carbon::class, $sut->getAttribute('start'));
        $this->assertEquals('2022-01-01 foo', $sut->getValueAsString());
    }

    #[Test]
    public function it_can_return_the_default_value(): void
    {
        $model = ['start' => ''];
        $sut = new Model($model, 'start');
        $sut->withDefaultValue(99);
        $this->assertEquals(99, $sut->getValueAsString());
    }
}
